# Count columns, to get best plot
columns = "`awk '{if(NR == 1) print NF}' < asd.dat`"
# For fewer columns, just plot the local vector
if (columns < 23) plot 'asd.dat' u 6:5 title 'Local'
# For more columns, plot the local vector with the color indicating the threat of the remote(s)
if (columns >= 23) plot 'asd.dat' u 6:5:23 lc var title 'Local', 'asd.dat' u 16:15 lc rgb 'orange' title 'Remote1', 'asd.dat' u 28:27 lc rgb 'orange' title 'Remote2'
