#!/bin/sh

# Manage the starting of both example1609 instances
# - Start appropriate GPS scripts on each machine
# - Start example1609 instances
# - Start UI simulator
# - Start gnuplot

# Script needs to be executed from the 'example1609' directory

# Have to configure remote

DIR=..

CURDIR=`pwd`
DATE=`date +"%y%m%d-%H%M%S"`

############################################################################## 
# functions
############################################################################## 

local_app_stop() 
{
  # Kill everything
  echo "Stopping local app..."
  sudo killall -q tcpdump
  sudo pkill -SIGINT example1609
  sleep 2
  sudo killall -q example1609
  sudo killall -q -9 tcpdump
  cd ../../../build/i686/debug/src/ && \
  gcovr --xml -r . | tee ${CURDIR}/raw.xml | sed 's,src.,,g' > ${CURDIR}/example1609-${DATE}-coverage.xml
}

local_app_start() 
{
  echo "Starting local app..."

  # Ensure GPS Rx fakes up-to-date timestamps
  (cd ${DIR}; grep -q 'Cohda_GPSRx_Sys_Time' pre.conf || printf "\nCohda_GPSRx_Sys_Time = 1;\n" >> pre.conf)
  # Increase debug level to at least get TDE log information for path plot
  (cd ${DIR}; sed -i 's/Cohda_DebugLevel.*= *[0-4];.*/Cohda_DebugLevel = 7; 0,255/' post.conf)

  # keep going until gpsd is running and talking to us
  while :
  do
    gpspipe -n 10 -r && break
    sleep 1
  done

  sleep 1

  # local app
  (cd ${DIR}; PYTHONPATH=. python cfg_convert.py -p obu_pre.conf -s obu_post.conf obu.cfg > example1609.conf)
  (cd ${DIR}; sudo env 'LD_LIBRARY_PATH=.' ./example1609 -a -f obu.cfg -c example1609.conf &)
}

local_gps_stop()
{
  echo "Stopping local GPS..."
  pkill -f gpsfake
}

local_gps_start()
{
  echo "Starting local GPS..."
  # start GPSD for local
  # use interval of 0.063 as this approximately equates to the 4 NMEA strings
  # at a rate of 4Hz
  gpsfake -c 0.063 ${DIR}/test/drives/drive-${TEST}-local.gps &
}

############################################################################## 
# main
############################################################################## 

PATH=/opt/cohda/bin:$PATH

case $1 in 
    "start")
      shift
      # get test number
      TEST=01
      if [ $# -gt 0 ]
      then TEST=$1
      fi
      # Start the GPS fakes close together
      local_gps_start $*
      # Start the apps
      local_app_start $*
      ;;
      
    "stop")
      local_app_stop
      local_gps_stop
      ;;
      
    "prep")
      sudo rm -rf /mnt/ubi/log ; sudo mkdir -p /mnt/ubi/log
      sudo rm -rf /mnt/src/log ; sudo mkdir -p /mnt/src/log
      sudo apt-get -y install python-pygame
      sudo apt-get -y install python-gnuplot
      (cd ${DIR}/../../../; ./unload; ./load)
      make -C ${DIR}
      ;;

    *)
      echo "Usage: $0 [start|stop|prep]"
      ;;
esac

exit 0

