#!/bin/bash

# usage:
# $0 [logfile] [replot delay in sec]

logfile=/mnt/ubi/log/current/stderr
delay=0

if [ $# -gt 0 ]
then logfile=$1
fi

if [ $# -gt 1 ]
then delay=$2
fi

# Empty the data file
echo ' ' > asd.dat

# ensure no other gnuplot's
killall gnuplot

if [ $delay -ne 0 ]
then
  while [ `wc -l asd.dat | awk {'print $1'}` -lt 100 ]
  do
    #echo "waiting..."
    sleep 1
    # update data file
    grep 'TDE_WriteToLog:' $logfile | sed 's/TDE_WriteToLog: //' > asd.dat
  done

  # get data, ignoring first few samples as they can be erroneous
  grep 'TDE_WriteToLog:' $logfile | tail -n +10 | sed 's/TDE_WriteToLog: //' | sed '1,5d' > asd.dat

  # start a reloading plot
  gnuplot -e "load './test/tde-log-load.plt'" > /dev/null 2>&1 &

  while :
  do
    # update data
    grep 'TDE_WriteToLog:' $logfile | tail -n 100 | sed 's/TDE_WriteToLog: //' > asd.dat

    sleep $delay
  done

else
  # get data
  grep 'TDE_WriteToLog:' $logfile | sed 's/TDE_WriteToLog: //' | sed '1,5d' > asd.dat

  # start a one-shot plot
  gnuplot -e "load './test/tde-log.plt'" &
fi


