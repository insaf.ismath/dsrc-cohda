#!/usr/bin/env python

### NOTE that this will run on the MK2.
# Be careful which libraries you include


import types
import os
import sys
import libconfig
import copy

DEFAULT_FILES = (
    'pre.conf',
    'example.cfg',
    'post.conf',
    )

NODES = (
    'Cohda.IEEE1609.dot2',
    'Cohda.IEEE1609.dot3',
    'Cohda.IEEE1609.dot4.Control',
    'Cohda.IEEE1609.dot4.Service',
    'Cohda.IEEE1609.dot4.Safety',
    )

MAP = {
    'Cohda.IEEE1609.dot3.WSMP.Priority'         : 'BSMUserPriority',
    'Cohda.IEEE1609.dot3.WSMP.TxDataRate_Mbps'  : 'WSMP_DataRate',
    'Cohda.IEEE1609.dot3.WSMP.TxPwrLevel_dBm'   : 'WSMP_TxPower',
    'Cohda.IEEE1609.dot3.WBSS.Mode'             : 'WBSS_Service_Mode',
    'Cohda.IEEE1609.dot4.Control.ChannelNumber' : 'ForcedControlChanNum',
    'Cohda.IEEE1609.dot4.Safety.ChannelNumber'  : 'ContinuousChanNum',
    'Cohda.IEEE1609.dot4.Service.ChannelNumber' : 'ForcedSerChanNum',
    'Cohda.Provider.[0].WRA.IPv6Addr'           : 'WBSS_Service_0_IPAddress',
    'Cohda.Provider.[0].WRA.SvcPort'            : 'WBSS_Service_0_IPService',
    }

PREFIX = {
    'Cohda.IEEE1609.dot3.WSMP.'   : 'WSMP_',
    'Cohda.IEEE1609.dot4.Control.': 'CCH_',
    'Cohda.IEEE1609.dot4.Service.': 'SCH_',
}

IGNORE = (
    'Cohda.IEEE1609.dot2.CertificateStore',
    'Cohda.IEEE1609.dot2.MaxNumCerts',
    'Cohda.IEEE1609.dot2.RequestCertsLevel',
    )



class conf(libconfig.Config):
    def __init__(self):
        libconfig.Config.__init__(self)
        self.free = 'continue;'
        self.all_children = []

    def readFile(self, fn):
        libconfig.Config.readFile(self, fn)
        for node in NODES:
            self._parse(node)

    def getSimpletype(self, instance):
        if self.getType(instance) == libconfig.Config.TypeFloat:
            t = 'float'
        elif self.getType(instance) == libconfig.Config.TypeBoolean:
            t = 'bool'
        elif self.getType(instance) == libconfig.Config.TypeInt:
            t = 'uint32_t'
        elif self.getType(instance) == libconfig.Config.TypeInt64:
            t = 'uint64_t'
        elif self.getType(instance) == libconfig.Config.TypeString:
            t = 'char_p'
        else:
            raise TypeError("instance is <%s> not a simpletype" % self.getType(instance))
        return t

    def _parse(self, root):
        children = self.children(root)
        if (self.isScalar(root)):
            self.all_children.append((root, self.getType(root), self.value(root)[0]))
        for i in children:
            self._parse(i)

def dump(filename, fileHandle = sys.stdout):
    if fileHandle:
        try:
            fileHandle.write('#include <%s>\n' % filename)
            fileHandle.write(open(filename,'r').read())
            fileHandle.write('#end <%s>\n' % filename)
        except:
            fileHandle.write('#oops <%s>\n' % filename)


def usage():
    print('"%s"          to use defaults %s' % (sys.argv[0],str(DEFAULT_FILES)))
    print('"%s conf"     to use default pre/post' % sys.argv[0])
    print('"%s pre conf" to use default post' % sys.argv[0])
    print('"%s" pre conf post"' % sys.argv[0])
    sys.exit(1)

def main():
    if len(sys.argv) == 1:
        files = DEFAULT_FILES
    elif len(sys.argv) == 2:
        files = (DEFAULT_FILES[0], sys.argv[1], DEFAULT_FILES[2])
    elif len(sys.argv) == 3:
        files = (sys.argv[1], sys.argv[2], DEFAULT_FILES[2])
    elif len(sys.argv) == 4:
        files = tuple(sys.argv[1:4])
    else:
        usage()

    for f in files:
        if not os.access(f, os.O_RDONLY):
            print('cannot read "%s"' % f)
            usage()

    dump(files[0])
    print('#convert  <%s>\n' % files[1])
    c = conf()
    c.readFile(files[1])
    for (key,typ,val) in c.all_children:
        # name = i[0]
        name = key.split('.')[-1]
        value = str(val)

        # Prefixes
        for p in PREFIX:
            if (key.startswith(p)):
                name = PREFIX[p] + name
        ## map
        if key in MAP:
            name = MAP[key]
        ## ignore list
        elif key in IGNORE:
            continue
        print('{0:<25s} = {1:>18s} # {2:s}'.format(name, value, key))

    print('\n#Signed PSIDs')

    for (i,n) in enumerate(c.children('Cohda.Forwarding.Signed')):
        print('Cohda_Additional_Secure_PSID_%d = 0x%X' % (i, c.value(n)[0]))

    print('\n#end  <%s>\n' % files[1])

    dump(files[2])

if __name__ == '__main__':
    main()
