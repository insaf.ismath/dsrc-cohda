#!/bin/sh

APPNAME=example1609

##############################################################################
# Self promote to root
##############################################################################
if [ -n "$EUID" ] && [ $EUID -ne 0 ] ; then
  CMD="$0 $@"
  exec sudo -E sh -c "$CMD"
fi

##############################################################################
# Parameter parsing
##############################################################################

# Optional 'Mode'
if [ $# -gt 1 ] ; then
  MODE=$2
fi

# Optional 'Extras'
if [ $# -gt 2 ] ; then
  EXTRA=$3
fi

##############################################################################
# Defaults
##############################################################################

# MODE controls which configurations are used. Default: obu
if [ -z "$MODE" ] ; then
  MODE="obu"
fi

if [ -z "$BOARD" ] ; then
  # try /proc/hwversion first, then check x86 and then fallback to hostname
  if [ -e /proc/hwversion ] ; then
    BOARD=`cat /proc/hwversion | cut -f 1 -d " "`
  elif [ `uname -m` = "i686" ] ; then
    BOARD=i686
  elif [ `uname -m` = "x86_64" ] ; then
    BOARD=x86_64
  elif [ `uname -m` = "i.MX6_Cohda_Wireless_MK5" ] ; then
    BOARD=MQ5
  elif [ `hostname` = "danlaw-DSRC" ] ; then
    BOARD=rdn9000
  else
    BOARD=`hostname`
  fi
fi

if [ -z "${APPDIR}" ] ; then
  case ${BOARD} in
    "MQ5")
      APPDIR=$(fullpath    $(dirname $0))
      ;;
    *)
      APPDIR=$(readlink -f $(dirname $0))
      ;;
  esac
  if [ -d ${APPDIR}/${APPNAME} ] ; then
    APPDIR="${APPDIR}/${APPNAME}"
  fi
fi

if [ -z "$PERSISTDIR" ] ; then
  case $BOARD in
    "MK5" | "MQ5")
      PERSISTDIR="/mnt/rw/${APPNAME}"
      ;;
    *)
      PERSISTDIR=`pwd`
      ;;
  esac
fi

if [ -z "$PYDIR" ] ; then
  case $BOARD in
    "MK2" | "MK2r1")
      PYDIR="/lib/ld-linux.so.3 --library-path /mnt/ubi/new_root/usr/lib /mnt/ubi/new_root/usr/bin/"
      ;;
    *)
      PYDIR=
      ;;
  esac
fi

if [ "${EXTRA}" = "valgrind" ] ; then
  WRAPPER="valgrind --log-file=${APPDIR}/valgrind_output.txt --tool=memcheck --leak-check=yes -v"
  KILLCMD="valgrind.bin"
fi

if [ "${EXTRA}" = "strace" ] ; then
  WRAPPER="strace -tt -f -o ${APPDIR}/strace_output.txt"
  KILLCMD="./${APPNAME}"
fi

##############################################################################
# Functions
##############################################################################

_pidof()
{
  case $BOARD in
    "MQ5")
        PIDS=$(grep    "${1}" /proc/[1-9]*/cmdline 2>/dev/null | tr '\0' '\n' | grep cmdline | grep -v grep | grep -v "rc.${1}" | awk -F/ '{print $3}' | tr '\n' ' ')
      ;;
    *)
      if [ -x "$(command -v pidof)" ]; then
        PIDS=$(pidof   "${1}")
      else
        PIDS=$(grep -a "${1}" /proc/[1-9]*/cmdline 2>/dev/null | tr '\0' ' '  |                grep -v grep | grep -v "rc.${1}" | awk -F/ '{print $3}' | tr '\n' ' ')
      fi
      ;;
  esac

  echo ${PIDS}
}

_killall()
{
  PIDS=$(_pidof ${1})
  # 1st: Ask nicely
  if [ -n "${PIDS}" ] ; then
    for PID in "${PIDS}" ; do
      kill ${PID}
    done
  fi

  # Give it a chance to exit
  TIMEOUT=30
  while [ ${TIMEOUT} != 0 ] ; do
    PIDS=$(_pidof ${1})
    if [ -n "${PIDS}" ] ; then
      sleep 1;
      TIMEOUT=$(( ${TIMEOUT}-1 ))
    else
      TIMEOUT=0
    fi
  done

  # Still running? Insist!
  PIDS=$(_pidof ${1})
  if [ -n "${PIDS}" ] ; then
    for PID in "${PIDS}" ; do
      kill -9 ${PID}
    done
  fi
}

aerolink_cmd()
{
  # Exit quietly if there's no rc.aerolink file
  if [ ! -f ${APPDIR}/aerolink/rc.aerolink ] ; then
    return
  fi

  # Link to the persistent storage if present and local is missing
  if [ -d ${PERSISTDIR}/aerolink/active ] ; then
    if [ ! -d ${APPDIR}/aerolink/active ] ; then
      ln -fs ${PERSISTDIR}/aerolink/active ${APPDIR}/aerolink/active
    fi
  fi

  case "$1" in
    "" | "default")
      # Use the lack of a 'aerolink/active' directory as a proxy signal to automatically generate self signed *test* certificates
      # Caveat-emptor! This isn't bulletproof as the previously generated test certificates may have expired
      # If they have, one can manually regenerate the test certificates with: $0 aerolink clear
      # Also handle the case that previous aerolink setup was for a different mode by ensuring Aerolink is 'obu' or 'rsu' only
      if [ -d ${PERSISTDIR}/aerolink/active ] ; then
        case $MODE in
          "obu"|"emergency")
            if [ -e ${PERSISTDIR}/aerolink/active/.rsu ] ; then
              rm -rf ${PERSISTDIR}/aerolink/active
            fi
            ;;
          *)
            if [ -e ${PERSISTDIR}/aerolink/active/.obu ] ; then
              rm -rf ${PERSISTDIR}/aerolink/active
            fi
            ;;
        esac
      fi
      if [ ! -d ${PERSISTDIR}/aerolink/active ] ; then
        cd ${APPDIR}
          ./aerolink/rc.aerolink clear
          case $MODE in
            "obu"|"emergency")
              ./aerolink/rc.aerolink na-demo-obu
              touch ${APPDIR}/aerolink/active/.obu
              ;;
            *)
              ./aerolink/rc.aerolink na-demo-rsu
              touch ${APPDIR}/aerolink/active/.rsu
              ;;
          esac
        cd -
        # Copy it to the persistent storage directory when running in a non-persistent location (/opt/cohda/application)
        if [ ! ${PERSISTDIR}/aerolink -ef ${APPDIR}/aerolink ] ; then
          mkdir -p ${PERSISTDIR}/aerolink && \
            mv ${APPDIR}/aerolink/active ${PERSISTDIR}/aerolink/ &&
              ln -fs ${PERSISTDIR}/aerolink/active ${APPDIR}/aerolink/active
        fi
      fi
      ;;
    *)
      ${APPDIR}/aerolink/rc.aerolink $*
      ;;
  esac

}

app_stop()
{
  #stop_wsm_forwarding
  # Send the kill signal to the app and wait for it to exit
  _killall ${APPNAME}
  # Cleanup any tcpdump instances that may have been missed
  _killall tcpdump
}

app_start()
{
  # Make sure there are no rogue example1609 apps already running
  app_stop

  # Ensure networking is up before continuing
  case $BOARD in
    "MQ5")
      LOOPBACK_IF=lo0
      ;;
    *)
      LOOPBACK_IF=lo
      ;;
  esac
  while [ "$(ifconfig ${LOOPBACK_IF} | grep UP)" = "" ]; do
    echo "waiting for networking...";
    sleep 1;
  done

  # Wait until the time changes to later than build date (i.e. chronyd has set the time)
  case $BOARD in
    "MQ5")
      EPOCH=1514764800 # 1 Jan 2018
      while [ $(date -tu) -lt $EPOCH ] ; do date; sleep 1; done
      ;;
    *)
      EPOCH=$(stat -c%Z ${APPDIR}/${APPNAME})
      while [ $(date +%s) -lt $EPOCH ] ; do date; sleep 1; done
      ;;
  esac

  case $BOARD in
  "MK5" | "rdn9000")
    # Unload the kernel 1609 stack if the application includes the POSIX 1609 stack
    if ldd ${APPDIR}/${APPNAME} | grep -q "1609net" ; then
      rmmod ieee1609dot3 2>/dev/null
      rmmod ieee1609dot4 2>/dev/null
      rmmod ieee1609gnl  2>/dev/null
    fi
    ;;
  "MQ5")
    # Start mq if not already running
    PID=$(_pidof mq)
    if [ -z "${PID}" ] ; then
      mq
    fi
    ;;
  *)
    ;;
  esac

  # Prepare security
  aerolink_cmd default

  cd ${APPDIR}
    # Assume no .cfg -> .conf pocessing
    cat ${MODE}.conf > .${MODE}.conf
    if [ -f ${MODE}.conf.${BOARD} ] ; then
    cat ${MODE}.conf.${BOARD} >> .${MODE}.conf
    fi
    # Try the legacy (deprecated) .cfg -> .conf pocessing
    if [ -f cfg_convert.py ] ; then
      ${PYDIR}python cfg_convert.py -s ${MODE}.conf ${MODE}.cfg > .${MODE}.conf.legacy
      if [ $? -eq 0 ] ; then
        echo "Use legacy (deprecated) .cfg"
        mv .${MODE}.conf.legacy .${MODE}.conf
      fi
    fi

    LD_LIBRARY_PATH=./lib ${WRAPPER} ./${APPNAME} \
      -f ${MODE}.cfg \
      -c .${MODE}.conf \
      -C ${PERSISTDIR}/${MODE}.conf \
      -a &
    disown -h -a

    # Sleep a bit to make sure ${LOG}/current/stderr is in place when returning
    if [ -z "${WRAPPER}" ] ; then
      sleep 2
    else
      # When WRAPPER is defined, sleep longer
      sleep 5
    fi
  cd -

  #start_wsm_forwarding
}

start_wsm_forwarding()
{
  # Broadcast all WSM messages transmitted/received over UDP port 0x9090
  ip_addr_to_fwd='172.16.255.255'
  ip link set dev cw-mon-tx up
  ip link set dev cw-mon-rx up
  llc mcap -H 44 -i cw-mon-tx -r $ip_addr_to_fwd -m &
  llc mcap -H 68 -i cw-mon-rx -r $ip_addr_to_fwd -m &
}

stop_wsm_forwarding()
{
  _killall "llc mcap"
}


##############################################################################
# main
##############################################################################

PATH=/opt/cohda/bin:$PATH

case $1 in
  "start")
    app_start
    ;;
  "stop")
    app_stop
    ;;
  "restart")
    app_stop
    sleep 5
    app_start
    ;;
  "aerolink")
    shift # drop the 'aerolink' command, pass the remaining argument(s)
    aerolink_cmd $*
    ;;
  "ghs-sw")
    sed -i 's/crypto_provider .*/crypto_provider software/' ghs/config/aerolink.conf
    aerolink_cmd clear
    aerolink_cmd na-ghs-sw-load ./ghs
    aerolink_cmd check
    # Adjust the conf to match the aerolink setup
    sed -i 's/SecurityEnable .*/SecurityEnable = 1/' obu.conf
    sed -i 's/Dot2_MessageContextName .*/Dot2_MessageContextName = plugfest.wsc/' obu.conf
    ;;
  "ghs-hw")
    sed -i 's/crypto_provider .*/crypto_provider hardware/' ghs/config/aerolink.conf
    aerolink_cmd clear
    aerolink_cmd na-ghs-hw-load ./ghs
    aerolink_cmd check
    # Adjust the conf to match the aerolink setup
    sed -i 's/SecurityEnable .*/SecurityEnable = 1/' obu.conf
    sed -i 's/Dot2_MessageContextName .*/Dot2_MessageContextName = plugfest.wsc/' obu.conf
    ;;
  "scms-sw")
    aerolink_cmd default
    sed -i 's/crypto_provider .*/crypto_provider software/' aerolink/active/aerolink.conf
    sed -i 's/pki_type .*/pki_type          scms/' aerolink/active/aerolink.conf
    sed -i 's/SecurityEnable .*/SecurityEnable = 1/' obu.conf
    sed -i 's/Dot2_MessageContextName .*/Dot2_MessageContextName = exampleobe_scms.wsc/' obu.conf
    ;;
  "scms-hw")
    aerolink_cmd default
    sed -i 's/crypto_provider .*/crypto_provider hardware/' aerolink/active/aerolink.conf
    sed -i 's/pki_type .*/pki_type          scms/' aerolink/active/aerolink.conf
    sed -i 's/SecurityEnable .*/SecurityEnable = 1/' obu.conf
    sed -i 's/Dot2_MessageContextName .*/Dot2_MessageContextName = exampleobe_scms.wsc/' obu.conf
    ;;
  *)
    echo "Usage: $0 start|stop|restart <config-name>"
    echo "       $0 aerolink [cmd]"
    echo "       $0 ghs-sw # <- Sets up the Greenhills certificates for Plugfest in software mode"
    echo "       $0 ghs-hw # <- Sets up the Greenhills certificates for Plugfest in hardware mode"
    echo "       $0 scms # <- Sets up the SCMS environment"
    ;;
esac

exit 0
