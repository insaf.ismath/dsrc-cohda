# where files are stored to provide persistent data
Cohda_PersistanceDir = /opt/cohda/application/example1609

# Enable/disable EXT notification of LPH update
Cohda_LPH_Ext_Update         = 1

# 0: CONTINUOUS ACCESS FORCED
# 1: ALTERNATING ACCESS FORCED
# 2: ALTERNATING ACCESS CONDITIONAL
# 3: IMMEDIATE ACCESS CONDITIONAL
# 4: SINGLE RADIO

###################################################################
# V2X-Locate Core Configuration
###################################################################
Cohda_RPS.ENABLE = 0
# Debug level of V2X-Locate App
Cohda_RPS.DebugLevel = 4

# Location of configuration
Cohda_RPS.LOCATE_CONFIG_FILE_NAME = /opt/cohda/application/example1609/locate_config.json
# Per-access point configuration file location
Cohda_RPS.RPS_AP_MODE_APLIST_FILENAME = /opt/cohda/application/example1609/locate_aplist.json
# Newly discovered access point information file location
Cohda_RPS.RPS_AP_MODE_NEW_APLIST_FILENAME = /opt/cohda/application/example1609/locate_new_aplist.json

# Enable the GPS position output interface
Cohda_RPS.GPS_OUT_ENABLE = 0
# GPS position data interface host-name
Cohda_RPS.GPS_OUT_HOST   = 127.0.0.1
# GPS position data sent to this UDP port
Cohda_RPS.GPS_OUT_PORT   = 33992

# Locate output service, UDP output enable
Cohda_RPS.LOCATE_POSOUTPUT_ENABLE     = 0
# Locate output service UDP host name target
Cohda_RPS.LOCATE_POSOUTPUT_HOST       = 127.0.0.1
# Locate output service UDP port number
Cohda_RPS.LOCATE_POSOUTPUT_PORT       = 33994
# Locate output service PVT log enable
Cohda_RPS.LOCATE_POSOUTPUT_LOG_ENABLE = 1

# Log pcap for replay
Cohda_RPS.RAW_RANGING_ENABLE                              = 1
Cohda_RPS.RAW_RANGING_LOG_ENABLE                          = 1
# Log UBX for replay
Cohda_RPS.UBX_ENABLE                                      = 1
Cohda_RPS.UBX_PCAP_LOG_ENABLE                             = 1

###################################################################
# V2X-Locate GPS Configuration
###################################################################
Cohda_RPS.UBX_ENABLE = 1
# Specify the UBX device type
# This should match the installed hardware.
# 0 = M8N - basic GPS, NOT SUPPORTED AT THE MOMENT.
# 1 = M8U - UDR GPS ** Default for now
# 2 = M8L - ADR GPS system, with optional vstate speed support
Cohda_UBX.UBX_DEVICE_TYPE = 1

# Process HNR-PVT messages and configure UDR lever arm
Cohda_UBX_MODE     = 1
Cohda_UBX_LA_IMU_X = 0 # IMU lever arm X in cm
Cohda_UBX_LA_IMU_Y = 0 # IMU lever arm Y in cm
Cohda_UBX_LA_IMU_Z = 0 # IMU lever arm Z in cm
Cohda_UBX_LA_ANT_X = 0 # Antenna lever arm X in cm
Cohda_UBX_LA_ANT_Y = 0 # Antenna lever arm Y in cm
Cohda_UBX_LA_ANT_Z = 0 # Antenna lever arm Z in cm

###################################################################
# V2X-Locate Radio Configuration
###################################################################
# Radio number on which to operate (See @ref tMKxRadio)
Cohda_RPS.RPS_RADIO_NUM      = 0
# Logical channel number on which to operate (2 == auto selection based on config. See @ref tMKxChannel)
Cohda_RPS.RPS_CHANNEL_NUM    = 0
# Radio BADNWIDTH MODE, Default Mode = 10MHz
Cohda_RPS.RPS_BANDWIDTH_MODE = 10
# Default transmit power
Cohda_RPS.TX_DEFAULT_POWER   = 23
# Single Tx antenna for measuring low-level timing performance. - prevent beam-forming and pulsing of Tx power.
Cohda_RPS.TX_DEFAULT_ANTENNA = 3
# Locate RPS Ranging mode
Cohda_RPS.LOCATE_RPS_MODE = 1

##########################################################
# V2X-Locate radio timing configuration
##########################################################
# RPS ACK response delay adjustment, in picoseconds
Cohda_RPS.LOCATE_RPS_ACK_RESPONSE_DELAY_ADJUSTMENT_PS = 0
# Delay variance
Cohda_RPS.LOCATE_RPS_ACK_RESPONSE_DELAY_VARIANCE = 0
# Sync to the top of time slot
Cohda_RPS.RPS_LOCATE_SYNC_TO_TOP                 = 1
Cohda_RPS.RPS_LOCATE_SYNC_TO_TOP_OFFSET_ms       = 10

############################################################
# V2X-Locate Target management
############################################################
# Use devices broadcasting RPS annoucment frames as ranging targets
Cohda_RPS.TM_USE_RPS_ANNOUCEMENTS  = 0
# Use devices broadcasting IEEE1609 WSA's with the Cohda V2X-Locate
# PSID as ranging targets
Cohda_RPS.TM_USE_IEEE1609_WSAS     = 1
# Use unknown devices that either broadcast RPS announcements or
# IEEE1609 WSA's with the Cohda V2X-Locate PSID as ranging targets
Cohda_RPS.TM_TRUST_UNKNOWN_DEVICES = 1

# Enable if Transmission State is not available
Cohda_RPS.LOCATE_VEHICLE_DIRECTION_FROM_IMU = 1;0,1
