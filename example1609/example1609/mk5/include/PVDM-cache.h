#pragma once
/**
 * @addtogroup app_pvdm Probe Vehicle Snapshot Cache
 * @{
 *
 * @file
 */

//------------------------------------------------------------------------------
// Copyright (c) 2016 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __PROBE_VEHICLE_SNAPSHOT_CACHE_MESSAGE_H__
#define __PROBE_VEHICLE_SNAPSHOT_CACHE_MESSAGE_H__

// ----------------------------------------------------------------------------
// Includes
// ----------------------------------------------------------------------------
#include "j2735asn.h"
#include "queue.h"

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------
/// Snapshot Type
typedef enum
{
  PVD_SNAPSHOT_EVENT = 1,
  PVD_SNAPSHOT_STARTSTOP,
  PVD_SNAPSHOT_PERIODIC,
} ePVDSnapshotType;

/// @copydoc ePVDSnapshotType
typedef uint8_t tPVDSnapshotType;

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------
struct PVDMContainer;

// specify the list head structure
// This contains a pointer to first and last elements
TAILQ_HEAD(SnapshotHead, SnapshotList);

typedef struct SnapshotList
{
  TAILQ_ENTRY(SnapshotList) List;

} tSnapshotList;

typedef struct SnapshotListItem
{
  // List entry to the snapshot list
  struct SnapshotList List;

  // The actual snapshot
  SAESnapshot *pSnapshot;

  // The associated Probe Segment Number
  int16_t PSN;
} tSnapshotListItem;

typedef struct PVDMSnapshotCacheState
{
  // Periodic removal index
  int RemovalIndex;

} tPVDMSnapshotCacheState;

typedef struct PVDMSnapshotCache
{
  // Total number of elements
  // Should be the sum of all Num* below
  int NumSnapshots;

  // Periodic snapshots
  int NumPeriodicSnapshots;
  struct SnapshotHead PeriodicSnapshots;

  // Start/stop snapshots
  int NumStartStopSnapshots;
  struct SnapshotHead StartStopSnapshots;

  // Event snapshots
  int NumEventSnapshots;
  struct SnapshotHead EventSnapshots;
} tPVDMSnapshotCache;

//------------------------------------------------------------------------------
// Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------
int PVDMCache_Init(struct PVDMContainer *pPVDM);
int PVDMCache_Deinit();
int PVDMCache_AddSnapshot(struct PVDMContainer *pPVDM,
                          SAESnapshot *pSnapshot,
                          tPVDSnapshotType Type);
int PVDMCache_RemoveSnapshot(struct PVDMContainer *pPVDM,
                             SAESnapshot *pSnapshot,
                             tPVDSnapshotType Type);
tSnapshotListItem *PVDMCache_GetSnapshot(struct PVDMContainer *pPVDM,
                                         tPVDSnapshotType);


#if defined(UNITTEST)
int UNITTEST_PVDMCache_RemovePeriodicSnapshot(struct PVDMContainer *pPVDM);
int UNITTEST_PVDMCache_RemoveStartStopSnapshot(struct PVDMContainer *pPVDM);
int UNITTEST_PVDMCache_RemoveEventSnapshot(struct PVDMContainer *pPVDM);
#endif

#ifdef __cplusplus
}
#endif

#endif // __PROBE_VEHICLE_SNAPSHOT_CACHE_MESSAGE_H__

/**
 * @}
 */
