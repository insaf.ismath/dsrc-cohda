/**
 * @addtogroup ets_app_facilities_toll ITS Facilities Toll support
 * @{
 *
 * ETSI Toll functionality
 *
 * @file
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2015 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __ETSI_TOLL__ETSI_TOLL_H_
#define __ETSI_TOLL__ETSI_TOLL_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// Format for 'blob' in toll station entry, containing info on toll station
typedef struct TollLocation
{
  /// Station ID  0-10000
  uint16_t Id;
  /// Station Attributes (Need definition, leave all 0 for now)
  union
  {
    uint16_t Attributes;
    struct
    {
      /// Reduced Power Level
      int8_t TxPower;
      /// Range to reduce power if not specified in location [10m]
      uint8_t Radius;
    };
  };
  /// Latitude [microdegrees]
  int32_t Latitude;
  /// Longitude [microdegrees]
  int32_t Longitude;
  /// Height above ellipsoid, FFFF = any height [1m]
  int16_t Height;
} __attribute__ ((packed)) tTollLocation;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

void ETSIToll_Init (void);
void ETSIToll_Deinit (void);

#ifdef __cplusplus
}
#endif

#endif//__ETSI_TOLL__ETSI_TOLL_H_

// Close the Doxygen group
/**
 * @}
 */
