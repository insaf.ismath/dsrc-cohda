/**
 * @addtogroup ets_app_facilities_map_api ETSI ITS Facilities MAP API
 * @{
 *
 * Interface to MAP handling
 *
 * @code
 *
 * #include "itsasn.h"
 * #include "itsasn_def.h"
 * #include "ext.h"
 *
 * void MAPRxExtCallback(tExtEventId Event,
 *                       tEXT_Message *pData,
 *                       void *pPriv)
 * {
 *   (void)pPriv; // My private data
 *
 *   switch (Event)
 *   {
 *     case QSMSG_EXT_RX_ITSFL_PDU:
 *     {
 *       // Check PDU header
 *       if (pData->pPDU->messageID == ITSItsPduHeaderMessageID_ID_mapem)
 *       {
 *         ITSMAPEM *pMapPdu = pData->pMAP;
 *
 *         // Parse some data
 *         printf("Station is 0x%08x\n", pMapPdu->header.stationID);
 *
 *         // Send MAP to handling application
 *       }
 *     }
 *
 *     default:
 *       // Other events
 *       break;
 *   }
 * }
 *
 * @endcode
 *
 * @file
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2017 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __ETSI_MAP_ETSI_MAP_IF_H_
#define __ETSI_MAP_ETSI_MAP_IF_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#include <stdint.h>
#include "etsi-fac-common.h"

#include "itsasn.h"
#include "itsasn_def.h"

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// MAP management
typedef struct MAPMgmt
{
  /// Repetition Duration (sec), ETSIFAC_REPETITIONDURATIONNONE for none
  uint32_t RepetitionDuration;
  /// Repetition Interval (mS), ETSIFAC_REPETITIONINTERVALNONE for none
  uint16_t RepetitionInterval;
  // NOTE: Both RepetitionDuration and RepetitionInterval need to be non-zero for repetition
  // NOTE: To immediately terminate a repetition, set RepetitionDuration to ETSIFAC_REPETITIONDURATIONNONE
  //       and RepetitionDuration to ETSIFAC_REPETITIONINTERVALTERMINATE

  /// MAP structure
  const ITSMapData *pMap;

  /// GN Packet transport type @ref eETSIFACGeoNetTransport
  tETSIFACGeoNetTransport PktTransport;

  /// Destination Area (only required for GBC PktTransport)
  tETSIFACGeoNetArea DestArea;

  /// Comms Profile
  tETSIFACGNProfile CommsProfile;
  /// Traffic Class
  tETSIFACGNTC TrafficClass;
  /// HopLimit
  tETSIFACGNHopLimit HopLimit;
} tMAPMgmt;

/// MAP Status codes
typedef enum MAPStatusCode
{
  /// MAP success
  ETSIMAP_SUCCESS = 0x00,
  /// MAP generation failure
  ETSIMAP_FAILURE = 0x01,
  /// MAP some parameters invalid
  ETSIMAP_FAILURE_INVALID_PARAMS = 0x04,
  /// MAP limits exceeded in structure
  ETSIMAP_FAILURE_CONSTRAINT = 0x05,
  /// MAP failed to encode
  ETSIMAP_FAILURE_ENCODING = 0x06,
  /// MAP generation not ready
  ETSIMAP_FAILURE_NOT_READY = 0x07,

} eMAPStatusCode;

/// Status code for MAP actions @ref eMAPStatusCode
typedef uint8_t tMAPStatusCode;

/// Action ID
typedef uint16_t tMAPIdNum;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

/// @brief Trigger periodic MAP
/// @param pMgmt Pointer to MAP management structure
/// @param IdNum Identifier of MAP to handle
/// @return MAP Status Code
tMAPStatusCode ETSIMAP_SendMAP(const tMAPMgmt *pMgmt, tMAPIdNum IdNum);

#ifdef __cplusplus
}
#endif

#endif // __ETSI_MAP_ETSI_MAP_IF_H_

// Close the Doxygen group
/**
 * @}
 */
