/**
 * @addtogroup ets_app_facilities_rtcm_api ETSI ITS Facilities RTCM API
 * @{
 *
 * Interface to RTCM handling
 *
 * @code
 *
 * #include "itsasn.h"
 * #include "itsasn_def.h"
 * #include "ext.h"
 *
 * void RTCMRxExtCallback(tExtEventId Event,
 *                        tEXT_Message *pData,
 *                        void *pPriv)
 * {
 *   (void)pPriv; // My private data
 *
 *   switch (Event)
 *   {
 *     case QSMSG_EXT_RX_ITSFL_PDU:
 *     {
 *       // Check PDU header
 *       if (pData->pPDU->messageID == ITSItsPduHeaderMessageID_ID_rtcmem)
 *       {
 *         ITSRtcmPdu *pRtcmPdu  = pData->rtcm;
 *
 *         // Parse some data
 *         printf("Station is 0x%08x\n", pRtcmPdu->header.stationID);
 *
 *         // Send RTCM to handling application
 *       }
 *     }
 *
 *     default:
 *       // Other events
 *       break;
 *   }
 * }
 *
 * @endcode
 *
 * @file
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2018 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __ETSI_RTCM_ETSI_RTCM_IF_H_
#define __ETSI_RTCM_ETSI_RTCM_IF_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#include <stdint.h>
#include "etsi-fac-common.h"

#include "itsasn.h"
#include "itsasn_def.h"

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// RTCM management
typedef struct RTCMMgmt
{
  /// Repetition Duration (sec), ETSIFAC_REPETITIONDURATIONNONE for none
  uint32_t RepetitionDuration;
  /// Repetition Interval (mS), ETSIFAC_REPETITIONINTERVALNONE for none
  uint16_t RepetitionInterval;
  // NOTE: Both RepetitionDuration and RepetitionInterval need to be non-zero for repetition
  // NOTE: To immediately terminate a repetition, set RepetitionDuration to ETSIFAC_REPETITIONDURATIONNONE
  //       and RepetitionDuration to ETSIFAC_REPETITIONINTERVALTERMINATE

  /// RTCM structure
  const ITSRTCMcorrections *pRtcm;

  /// GN Packet transport type @ref eETSIFACGeoNetTransport
  tETSIFACGeoNetTransport PktTransport;

  /// Destination Area (only required for GBC PktTransport)
  tETSIFACGeoNetArea DestArea;

  /// Comms Profile
  tETSIFACGNProfile CommsProfile;
  /// Traffic Class
  tETSIFACGNTC TrafficClass;
  /// HopLimit
  tETSIFACGNHopLimit HopLimit;
} tRTCMMgmt;

/// RTCM Status codes
typedef enum RTCMStatusCode
{
  /// RTCM success
  ETSIRTCM_SUCCESS = 0x00,
  /// RTCM generation failure
  ETSIRTCM_FAILURE = 0x01,
  /// RTCM some parameters invalid
  ETSIRTCM_FAILURE_INVALID_PARAMS = 0x04,
  /// RTCM limits exceeded in structure
  ETSIRTCM_FAILURE_CONSTRAINT = 0x05,
  /// RTCM failed to encode
  ETSIRTCM_FAILURE_ENCODING = 0x06,
  /// RTCM generation not ready
  ETSIRTCM_FAILURE_NOT_READY = 0x07,

} eRTCMStatusCode;

/// Status code for RTCM actions @ref eRTCMStatusCode
typedef uint8_t tRTCMStatusCode;

/// Action ID
typedef uint16_t tRTCMIdNum;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

/// @brief Trigger periodic RTCM
/// @param pMgmt Pointer to RTCM management structure
/// @param IdNum Identifier of RTCM to handle
/// @return RTCM Status Code
tRTCMStatusCode ETSIRTCM_SendRTCM(const tRTCMMgmt *pMgmt, tRTCMIdNum IdNum);

#ifdef __cplusplus
}
#endif

#endif // __ETSI_RTCM_ETSI_RTCM_IF_H_

// Close the Doxygen group
/**
 * @}
 */
