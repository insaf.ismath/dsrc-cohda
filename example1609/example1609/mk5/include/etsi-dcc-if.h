/**
 * @addtogroup v2x_etsi_dcc ETSI ITS DCC elements
 * @{
 *
 * @file
 *
 * ETSI DCC Public interface
 *
 * Provides some essential DCC information
 *
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2015 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __ETSI_DCC_IF_H_
#define __ETSI_DCC_IF_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

/// Invalid value for State number
#define DCCIF_STATE_INVALID 0xFF

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// Channel access
typedef enum
{
  /// CCH
  ETSI_DCCIF_CCH = 0,
  /// SCH
  ETSI_DCCIF_SCH = 1,
} tETSIDCCIF_ChannelType;


//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

/**
 * @brief Get last CBR (lastChannelLoad) for the given channel
 *
 * @param ChType CCH or SCH channel
 * @return The last channel load measurement in deci-percent
 */
uint16_t ETSIDCC_GetLastChannelLoad(tETSIDCCIF_ChannelType ChType);

/**
 * @brief Get average CBR (avgChannelLoad) for the given channel
 *
 * @param ChType CCH or SCH channel
 * @return The average channel load measurement in deci-percent
 */
uint16_t ETSIDCC_GetAvgChannelLoad(tETSIDCCIF_ChannelType ChType);

/**
 * @brief Get Current DCC state for the given channel
 *
 * @param ChType CCH or SCH channel
 * @return The current DCC state (0=RELAXED, 1 to n=ACTIVE, n+1=RESTRICTIVE)
 *         Where n is obtained from @sa ETSIDCC_GetNumActiveState
 *         Or DCCIF_STATE_INVALID for invalid request
 */
uint8_t ETSIDCC_GetCurrentState(tETSIDCCIF_ChannelType ChType);

/**
 * @brief Get number of 'active' DCC states for the given channel
 *
 * @param ChType CCH or SCH channel
 * @return The number of active DCC states
 *         Or DCCIF_STATE_INVALID for invalid request
 */
uint8_t ETSIDCC_GetNumActiveState (tETSIDCCIF_ChannelType ChType);

#ifdef __cplusplus
}
#endif

#endif // __ETSI_DCC_IF_H_

/**
 * @}
 */
