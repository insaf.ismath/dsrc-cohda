/**
 * @addtogroup util_pktbuf ETSI ITS Network packet buffers
 * @{
 *
 * @section util_pktbuf_ ETSI stack zero copy packet buffers
 *
 * @subsection util_pktbuf_tx Transmit packets
 *  - Allocate with PktBuf_Alloc()
 *   - Zero sizes are recorded in each 'Len' element of tUtilPktBufEntry (except payload)
 *  - Each stack element should update their relevant element:
 *   - CAM/DENM shall populate DescTransport  & update Payload
 *   - BTP shall clear DescTransport, set HdrTransport, populate DescNetwork  & update Payload
 *   - GN shall clear DescNetwork, set HdrNetwork, populate DescLink  & update Payload
 *   - G5 shall clear DescLink, populate DescMedium(MK2TxDescriptor + EthHdr) & update Payload before calling sendmsg() [dot4]
 *                              populate HdrLLC & update Payload before calling TxReq [LLC]
 *
 * @subsection util_pktbuf_rx Receive packets
 *  - Allocate with PktBuf_Alloc()
 *   - Zero sizes are recorded in each 'Len' element of tUtilPktBufEntry (except payload)
 *  - Each stack element should update their relevant element:
 *   - G5 shall set DescMedium (MK2RxDescriptor + EthHdr) before calling recvmsg() [dot4]
 *              set HdrLLC [LLC]
 *   - G5 shall clear DescMedium, set DescLink & update Payload
 *   - GN shall clear DescLink, set HdrNetwork, populate DescNetwork & update Payload
 *   - BTP shall clear DescNetwork, set HdrTransport, populate DescTransport & update Payload
 *   - CAM/DENM shall clear DescTransport, set DescApplication (optional) & update Payload
 *
 * @note It is the resposibility of each layer to check that they don't exceed UTIL_PKTBUF_*_MAX
 *
 * @file
 *
 * ETSI stack zero copy packet buffer headers
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2011 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __UTIL_PACKET_BUFFER_H_
#define __UTIL_PACKET_BUFFER_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------
#include <stdint.h>
#include <stdbool.h>

#include <sys/uio.h>

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

// Keep them multiples of 8 or 16
/// Maximum size of over the air application headers
#define UTIL_PKTBUF_HDR_APP_MAX  (16)
/// Maximum size of over the air transport headers
#define UTIL_PKTBUF_HDR_TP_MAX   (8)
/// Maximum size of over the air network headers
#define UTIL_PKTBUF_HDR_NET_MAX  (96)
/// Maximum size of over the air link headers
#define UTIL_PKTBUF_HDR_LLC_MAX  (48)
/// Maximum size of over the air MAC headers
#define UTIL_PKTBUF_HDR_MAC_MAX  (0)
/// Maximum size of over the air PHY headers
#define UTIL_PKTBUF_HDR_PHY_MAX  (0)

/// Maximum size of application descriptors
#define UTIL_PKTBUF_DESC_APP_MAX (32)
/// Maximum size of transport descriptors
#define UTIL_PKTBUF_DESC_TP_MAX  (144)
/// Maximum size of network descriptors
#define UTIL_PKTBUF_DESC_NET_MAX (144)
/// Maximum size of link descriptors
#define UTIL_PKTBUF_DESC_LLC_MAX (64)
/// Maximum size of MAC descriptors
#define UTIL_PKTBUF_DESC_MAC_MAX (48)
/// Maximum size of PHY descriptors
#define UTIL_PKTBUF_DESC_PHY_MAX (0)

/// Maximum size of packet buffer memory (everything)
#define UTIL_PKTBUF_MAX  (2100)

/// mask bits for obtaining packet size
#define UTIL_PKTBUF_MASK_DescAppl       (1 << 0)
#define UTIL_PKTBUF_MASK_DescTransport  (1 << 1)
#define UTIL_PKTBUF_MASK_DescNetwork    (1 << 2)
#define UTIL_PKTBUF_MASK_DescLink       (1 << 3)
#define UTIL_PKTBUF_MASK_DescMedium     (1 << 4)
#define UTIL_PKTBUF_MASK_HdrLLC         (1 << 5)
#define UTIL_PKTBUF_MASK_HdrNetwork     (1 << 6)
#define UTIL_PKTBUF_MASK_HdrTransport   (1 << 7)
#define UTIL_PKTBUF_MASK_ClearText      (1 << 8)
#define UTIL_PKTBUF_MASK_SignedText     (1 << 9)
#define UTIL_PKTBUF_MASK_Payload        (1 << 10)

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

typedef enum
{
  /// Buffer allocated, but not used
  PKTBUF_LAYER_NONE,
  /// ACCESS (G5)
  PKTBUF_LAYER_ACCESS,
  /// NETWORK (GN)
  PKTBUF_LAYER_NETWORK,
  /// NETWORK (GN) Forwarding
  PKTBUF_LAYER_NETWORK_FWD,
  /// TRANSPORT (BTP)
  PKTBUF_LAYER_TRANSPORT,
  /// FACILITIES (FAC)
  PKTBUF_LAYER_FACILITIES,
  /// CLASSIFICATION (TC)
  PKTBUF_LAYER_CLASSIFICATION,
  /// APPLICATION (APP)
  PKTBUF_LAYER_APPLICATION,
} eUtilPktBufLayer;

/// @copydoc eUtilPktBufLayer
typedef uint8_t tUtilPktBufLayer;

typedef enum
{
  /// Media unknown
  PKTBUF_MEDIA_UNKNOWN,
  /// ITS-G5
  PKTBUF_MEDIA_ITSG5,
  /// Ethernet
  PKTBUF_MEDIA_ETHERNET,
  /// Cellular-V2X
  PKTBUF_MEDIA_CV2X,
  /// UDP
  PKTBUF_MEDIA_UDP,
  /// File
  PKTBUF_MEDIA_FILE,
} eUtilPktBufMedia;

/// @copydoc eUtilPktBufMedia
typedef uint8_t tUtilPktBufMedia;

/// Buffer entry (pointer/length tuple)
/// Deliberately maps to @c struct iovec
typedef struct UtilPktBufEntry
{
  /// Buffer pointer
  uint8_t *pData;
  /// Buffer length
  size_t Len;
} tUtilPktBufEntry;

/// Table of buffer entries
typedef struct UtilPktBufTable
{
  /// Appl descriptor (Appl)
  struct UtilPktBufEntry DescAppl;
  /// Transport descriptor (FAC<->BTP)
  struct UtilPktBufEntry DescTransport; ///< @sa tETSIBTPHdr
  /// Network descriptor (BTP<->GN)
  struct UtilPktBufEntry DescNetwork; ///< @sa tETSIGeoNetHdr
  /// Link descriptor (GN<->G5)
  struct UtilPktBufEntry DescLink; ///< @sa tETSIG5Hdr
  /// MAC descriptor (G5<->MKX/Radio)
  struct UtilPktBufEntry DescMedium; ///< @sa tG5XTxDescriptor @sa tG5XRxDescriptor + struct ethhdr

  /// Over-the-air media-specific header (802.11/PC5)
  struct UtilPktBufEntry HdrLLC; ///< @sa tIEEE80211QoSHeader + tSNAPHeader
  /// Over-the-air network header (GN)
  struct UtilPktBufEntry HdrNetwork; ///< @sa tETSIGeoNetPktHdr
  /// Over-the-air transport header (BTP)
  struct UtilPktBufEntry HdrTransport; ///< @sa tETSIBTPPktHdr

  /// Over-the-air clear (pre-signed) data
  struct UtilPktBufEntry ClearText;
  /// Over-the-air signed data
  struct UtilPktBufEntry SignedText;
  /// Over-the-air payload data
  struct UtilPktBufEntry Payload;

} tUtilPktBufTable;

#define NUM_PKT_BUF_ENTRIES  \
  (sizeof(tUtilPktBufTable) / sizeof(struct UtilPktBufEntry))

/// Packet buffer
/// sendmsg() iovec = DescMedium + HdrNet + HdrTran + SignedText
/// recvmsg() iovec = DescMedium + SignedText
typedef struct UtilPacketBuffer
{
  /// Mem location to free
  void *pFree;
  /// free() function to use
  void (*pFunc)(void *);

  union {
    /// IO vector used in the sendmsg() call
    struct iovec Vector[NUM_PKT_BUF_ENTRIES];
    /// Table of elements (to mirror Vector)
    struct UtilPktBufEntry Tbl[NUM_PKT_BUF_ENTRIES];
    /// Table of user freindly elements
    struct UtilPktBufTable Elem;
  };

  /// Identity
  uint32_t Identity;
  /// Layer status
  tUtilPktBufLayer Layer;
  /// Media status
  tUtilPktBufMedia Media;

  union {
    struct {
      /// Debug controls
               /// Drop level
               /// 1 = error, 2 += unusual, 3 += normal
      uint16_t DebugDrop :2;
               /// Dump level
               /// 1 = payload, 2 += header, 3 += descriptor
      uint16_t DebugDump :2;
               /// Send level
               /// 1 = between layer, 2 += internal
      uint16_t DebugSend :2;

               /// LocT level
               /// 1 = add/remove, 2 += periodic, 3 += update,
      uint16_t DebugLocT :2;
               /// LDM level
               /// 1 = add/remove, 2 += periodic, 3 += update,
      uint16_t DebugFacCache :2;
               /// LDM level
               /// 1 = add/remove, 2 += periodic, 3 += update,
      uint16_t DebugLDM :2;
               /// App Level
               /// 1 = error, 2 += unusual, 3 += normal
      uint16_t DebugApp :2;
    };

    uint16_t DebugRaw;
  };


} tUtilPacketBuffer;


//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

struct UtilPacketBuffer *PktBuf_Alloc (size_t Len);
void                     PktBuf_Free (struct UtilPacketBuffer *pPtr);

struct UtilPacketBuffer *PktBuf_Dup  (const struct UtilPacketBuffer *pPtr);

size_t PktBuf_Size (const tUtilPacketBuffer * pPtr, uint32_t mask);

#ifdef __cplusplus
}
#endif

#endif // __UTIL_PACKET_BUFFER_H_

// Close the doxygen group
/**
 * @}
 */
