#pragma once
//------------------------------------------------------------------------------
// Copyright (c) 2018 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------
#include "conf_if.h"
#include "conf_common.h"

#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#ifdef __cplusplus
extern "C"
{
#endif


#define NA_C2C_Cohda_App_CongestionControlJ2945_DebugLevel_PARAM "Cohda_App_CongestionControlJ2945.DebugLevel"
#define NA_C2C_Cohda_App_CongestionControlJ2945_DebugLevel_MIN -1
#define NA_C2C_Cohda_App_CongestionControlJ2945_DebugLevel_DEF -1
#define NA_C2C_Cohda_App_CongestionControlJ2945_DebugLevel_MAX 255

#define NA_C2C_Cohda_App_CongestionControlJ2945_ENABLE_PARAM "Cohda_App_CongestionControlJ2945.ENABLE"
#define NA_C2C_Cohda_App_CongestionControlJ2945_ENABLE_DEF false

typedef struct NA_C2C_Cohda_App_CongestionControlJ2945
{
  /**
   * DebugLevel <- -1 <= -1 <= 255
   *## Congestion Control J2945 tuneable parameters
   * if != -1, override debug level for CongestionControlJ2945 module only
   */
  int32_t DebugLevel;

  /**
   * ENABLE <- false
   * ------------------CongestionControlJ2945 Relevant---------------------------------------------
   * CongestionControlJ2945 enabled?
   */
  bool ENABLE;
} tNA_C2C_Cohda_App_CongestionControlJ2945;

#define NA_C2C_Cohda_App_CongestionControlJ2945_NUM_SUB (0)
#define NA_C2C_Cohda_App_CongestionControlJ2945_NUM_FLD (2)

// compiled-in defaults
extern const tNA_C2C_Cohda_App_CongestionControlJ2945 NA_C2C_Cohda_App_CongestionControlJ2945_DEF;
// only for unit test...
const struct Conf_Callbacks * NA_C2C_Callbacks(void);
// get a R/O copy
const tNA_C2C_Cohda_App_CongestionControlJ2945 * NA_C2C_Get(void);
// get a R/W copy
tNA_C2C_Cohda_App_CongestionControlJ2945 * NA_C2C_Clone(void);
// Release a R/O copy
void NA_C2C_Release(const tNA_C2C_Cohda_App_CongestionControlJ2945 ** ppNA_C2C_Cohda_App_CongestionControlJ2945);
// Update, leave pNA_C2C_Cohda_App_CongestionControlJ2945 intact
void NA_C2C_Put(const tNA_C2C_Cohda_App_CongestionControlJ2945 * pNA_C2C_Cohda_App_CongestionControlJ2945);
// Update, clean up *ppNA_C2C_Cohda_App_CongestionControlJ2945
void NA_C2C_Push(tNA_C2C_Cohda_App_CongestionControlJ2945 ** ppNA_C2C_Cohda_App_CongestionControlJ2945);
void NA_C2C_Cohda_App_CongestionControlJ2945_Dump(FILE * pFile, const tNA_C2C_Cohda_App_CongestionControlJ2945 * pDump, /*int MaxParamNameLen,*/ bool SkipDefault, bool SkipDisabled);
int  NA_C2C_Cohda_App_CongestionControlJ2945_Parse(const char * pKey, const char * pValue, tNA_C2C_Cohda_App_CongestionControlJ2945 * pParse, int Mode);
int  NA_C2C_Cohda_App_CongestionControlJ2945_Check(const tNA_C2C_Cohda_App_CongestionControlJ2945 * pCheck);
int  NA_C2C_Cohda_App_CongestionControlJ2945_Init(tNA_C2C_Cohda_App_CongestionControlJ2945 * pInit);
void NA_C2C_Cohda_App_CongestionControlJ2945_Clean(tNA_C2C_Cohda_App_CongestionControlJ2945 * pClean);
int  NA_C2C_Cohda_App_CongestionControlJ2945_Copy(tNA_C2C_Cohda_App_CongestionControlJ2945 * pDst, const tNA_C2C_Cohda_App_CongestionControlJ2945 * pSrc);
int  NA_C2C_Cohda_App_CongestionControlJ2945_Comp(const tNA_C2C_Cohda_App_CongestionControlJ2945 * pA, const tNA_C2C_Cohda_App_CongestionControlJ2945 * pB);
void NA_C2C_Cohda_App_CongestionControlJ2945_UpdateDebug(const tNA_C2C_Cohda_App_CongestionControlJ2945 * pRO, int DebugLevel);

#ifdef __cplusplus
}
#endif
