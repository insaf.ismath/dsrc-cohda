/**
 * @addtogroup ets_app_facilities_sa_api ETSI ITS Facilities SA API
 * @{
 *
 * Interface to SA handling
 *
 * @file
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2017 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __ETSI_SA_ETSI_SA_IF_H_
#define __ETSI_SA_ETSI_SA_IF_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#include <stdint.h>
#include "etsi-fac-common.h"

#include "itsasn.h"
#include "itsasn_def.h"

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

/// Max service infos per SaID
#define ETSISA_SERIVICE_INFO_MAX 4

/// Maximum SA application data size
#define ETSISA_APPDATAMAX 256

/// Maximum SA PSC size
#define ETSISA_PSCMAX 31

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// SA Request type
typedef enum ETSISAReqType
{
  /// Registration
  ETSISA_REQUEST_REGISTRATION,
  /// Update
  ETSISA_REQUEST_UPDATE,
  /// Deregistration
  ETSISA_REQUEST_DEREGISTRATION,
} eETSISAReqType;

/// SA Request type value @ref eETSISAReqType
typedef uint8_t tETSISAReqType;

/// SA Service
typedef struct SAService
{
  /// Id of protocol stack to be used, e.g. ITSProtocolType_ID_gN
  /// Use ITSProtocolType_ID_unknown to not include this item in SA
  uint8_t ProtId;

  /// Channel Index to be used, e.g. ITSChannelIndex_firstEntry
  /// Use ITSChannelIndex_notUsed to not include this item in SA
  uint8_t ChannelIndex;

  /// ITS-AID of service
  uint32_t ITSAID;

  /// Provider Service Context of service (optional, NULL for none)
  const uint8_t *pPSC;
  /// Provider Service Context of service len (optional, min 1, max ETSISA_PSCMAX)
  uint16_t PSCLen;

  /// Application data used when pApplicationData is not NULL and ApplicationDataLen is not 0
  /// SA application data
  const uint8_t *pApplicationData;
  /// SA application data length (max ETSISA_APPDATAMAX)
  uint16_t ApplicationDataLen;

  /// IPv6 parameters used when ITSProtocolType_ID_iPv6 is set
  /// SA IPv6 address
  uint8_t IPv6Addr[ITSIPv6Address_NumOctets];
  /// SA IPv6 port (optional, 0 for none)
  uint16_t IPv6Port;
} tSAService;

/// SA management
typedef struct SAMgmt
{
  /// Repetition Duration (sec), ETSIFAC_REPETITIONDURATIONNONE for none
  uint32_t RepetitionDuration;
  /// Repetition Interval (mS), ETSIFAC_REPETITIONINTERVALNONE for none
  uint16_t RepetitionInterval;
  // NOTE: Both RepetitionDuration and RepetitionInterval need to be non-zero for repetition
  // NOTE: To immediately terminate a repetition, set RepetitionDuration to ETSIFAC_REPETITIONDURATIONNONE
  //       and RepetitionDuration to ETSIFAC_REPETITIONINTERVALTERMINATE

  /// Request type
  tETSISAReqType ReqType;

  /// Number of services in this request
  uint8_t ServiceCount;
  /// Service Information
  tSAService Service[ETSISA_SERIVICE_INFO_MAX];

  /// Inclusion of Repeat Rate
  bool IncRptRate;
  /// Inclusion of 3D location
  bool Inc3DLoc;
  /// Advertiser Identifier (optional, NULL for none, max length ETSISA_PSCMAX)
  const char *pAdvID;

  /// Comms Profile
  tETSIFACGNProfile CommsProfile;
  /// Traffic Class
  tETSIFACGNTC TrafficClass;
} tSAMgmt;

/// SA Status codes
typedef enum SAStatusCode
{
  /// SA success
  ETSISA_SUCCESS = 0x00,
  /// SA generation failure
  ETSISA_FAILURE = 0x01,
  /// SA some parameters invalid
  ETSISA_FAILURE_INVALID_PARAMS = 0x04,
  /// SA failed to encode
  ETSISA_FAILURE_ENCODING = 0x05,
  /// SA generation not ready
  ETSISA_FAILURE_NOT_READY = 0x06,
  /// SA failure to modify services
  ETSISA_FAILURE_SERVICE_MOD = 0x07,
  /// SA failed permissions consistency
  ETSISA_FAILURE_PERMISSIONS = 0x08,

} eSAStatusCode;

/// Status code for SA actions @ref eSAStatusCode
typedef uint8_t tSAStatusCode;

/// saID
typedef uint8_t tSAIdNum;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

/// @brief Update periodic SA
/// @param pMgmt Pointer to SA management structure
/// @param SaID Identifier of SA
/// @return SA Status Code
tSAStatusCode ETSISA_ProviderUpdateSA(const tSAMgmt *pMgmt, tSAIdNum SaID);

#ifdef __cplusplus
}
#endif

#endif // __ETSI_SA_ETSI_SA_IF_H_

// Close the Doxygen group
/**
 * @}
 */
