/**
 * @addtogroup v2v_cfg_api Configuration service
 * @{
 *
 * @file
 *
 * CFG library API
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2010 Cohda Wireless Pty Ltd
//------------------------------------------------------------------------------

#ifndef __APPCORE_CONF_H__
#define __APPCORE_CONF_H__

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>

#include "id-global.h"
#include "conf-pcap.h"
#include "conf-ini.h"
#include "conf-cmd.h"

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

/// Supported messages in CONF
typedef enum
{
  /// Re-read config
  QSMSG_CONF_READ_CONFIG = QS_BASE_MSG_CONF,
} tConfMsgId;

/// Default file to override compile-time defaults
#define ETS_DEFAULTS_FILENAME  "./ets.conf"

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// VSC echo configuration (built from Cmd line opts).
typedef struct ConfigIniData
{
  /// Revision, incremented when configuration information is updated.
  int Revision;
  /// Reference Count.
  int RefCount;
  /// PCAP data
  tPcapMetaData PcapMeta;
  /// Information from the INI file.
  tIniFile IniFile;
} tConfigIniData;

typedef void (*tConfUpdatedCallback)(void *pData);

/// Protocol mode choices
typedef enum ProtocolMode
{
  /// Intially no protocol active
  PROTOCOL_MODE_NONE = -1,
  /// ETSI ITS Stack active
  PROTOCOL_MODE_ITS = 0,
  /// IEEE 1609 Stack active
  PROTOCOL_MODE_1609 = 1,
} eProtocolMode;

/// Protocol Mode @sa eProtocolMode
typedef int8_t tProtocolMode;

//------------------------------------------------------------------------------
// Function definitions
//------------------------------------------------------------------------------

/// CALL THIS FIRST!!!
int ConfInit(int Argc,
             char **ppArgv,
             tCmdConfig * CommandLine);

/// OR CALL THIS FIRST (for Library)
int ConfInitLib(const char *UserCfgFileName,
                const char *DefaultsFileName,
                tConfReadMode ReadMode,
                tConfUpdatedCallback ConfUpdatedCallback,
                void *pConfUpdatedData);

int Conf_ThreadInit(int schedulerPolicy, int schedulerPriority_pc);

void Conf_ThreadDeinit(void);

char *ConfGetFileName(void);

int ConfIniCheckForUpdates(void);

int ConfIniCheckFile(void);

void ConfDeInitLib(void);
void ConfDeInit(void);

/// Returns the initial, non-modifiable configuration structure
const tConfigIniData * ConfigInitial(void);

/// Returns the latest configuration structure - caller must release with
/// ConfigRelease() when done
const tConfigIniData * ConfigGet(void);

void ConfigRelease(const tConfigIniData *pConfig);

/// Returns the singleton configuration structure - caller must release with
/// ConfigReleaseIni() when done
const tIniFile * ConfigGetIni(void);

void ConfigReleaseIni(const tIniFile * pIni);

/// Returns the writeable configuration structure
tConfigIniData * ConfigModify(void);

/// Update with the given lines
int ConfigUpdate(const char * pLines, tConfReadMode mode);

/// Set some application-specific initial values
void ConfigPreSetDefaults(const char *pDefaults);

/// Updates the configuration with changed writeable configuration
int ConfigCommit(tConfigIniData *pConfig);

/// Override the debug level
int ConfOverrideDebugLevel(int Level,
                           const char *pHaystack,
                           const char *pNeedle);

// Anyone can query for Protocol Mode
tProtocolMode Conf_GetProtocolMode( void );

// Only active Protocol stack sets ProtocolMode
int Conf_SetProtocolMode( tProtocolMode Mode );

#ifdef __cplusplus
}
#endif

#endif

/**
 * @}
 */
