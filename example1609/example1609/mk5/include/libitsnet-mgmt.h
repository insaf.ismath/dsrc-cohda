/**
 * @addtogroup ets_libitsnet_mgmt ETSI ITS Network Layer Management API
 * @{
 *
 * Interface to manage ETSI ITS Network Layer
 *
 * @file
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2012 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __LIBITSNET_MGMT_H_
#define __LIBITSNET_MGMT_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// GN Addr configuration method
typedef enum
{
  LocalAddrConfMethod_auto = 1,
  LocalAddrConfMethod_managed = 2
} eLocalAddrConfMethod;

/// IsMobile
typedef enum
{
  IsMobile_stationary = 0,
  IsMobile_mobile = 1
} eIsMobile;

/// Geo NonArea forwarding algorithm types
typedef enum
{
  GeoNonAreaForwardingAlgorithm_unspecified = 0,
  GeoNonAreaForwardingAlgorithm_greedy = 1,
  GeoNonAreaForwardingAlgorithm_cbf = 2
} eGeoNonAreaForwardingAlgorithm;

/// Geo Area forwarding algorithm types
typedef enum
{
  GeoAreaForwardingAlgorithm_unspecified = 0,
  GeoAreaForwardingAlgorithm_simple = 1,
  GeoAreaForwardingAlgorithm_advanced = 2
} eGeoAreaForwardingAlgorithm;

/// Struct for GN MIB parameters
typedef struct libITS_GNMIBParams_tag
{
  /// GN_ADDR value
  uint64_t LocalGnAddr;
  /// Addr method
  eLocalAddrConfMethod LocalAddrConfMethod;
  /// Protocol version
  uint32_t ProtocolVersion;
  /// IsMobile
  eIsMobile IsMobile;
  /// MinimumUpdateFrequencyLPV
  uint16_t MinimumUpdateFrequencyLPV;
  /// MaxSduSize
  uint16_t MaxSduSize;
  /// MaxGeoNetworkingHeaderSize
  uint16_t MaxGeoNetworkingHeaderSize;
  /// LifetimeLocTE
  uint16_t LifetimeLocTE;
  /// LocationServiceMaxRetrans
  uint8_t LocationServiceMaxRetrans;
  /// LocationServiceRetransmitTimer
  uint16_t LocationServiceRetransmitTimer;
  /// LocationServicePacketBufferSize
  uint16_t LocationServicePacketBufferSize;
  /// BeaconServiceRetransmitTimer
  uint16_t BeaconServiceRetransmitTimer;
  /// BeaconServiceMaxJitter
  uint16_t BeaconServiceMaxJitter;
  /// DefaultHopLimit
  uint8_t DefaultHopLimit;
  /// MaxPacketLifetime
  uint16_t MaxPacketLifetime;
  /// MinPacketRepetitionInterval
  uint16_t MinPacketRepetitionInterval;
  /// GeoNonAreaForwardingAlgorithm
  eGeoNonAreaForwardingAlgorithm GeoNonAreaForwardingAlgorithm;
  /// GeoAreaForwardingAlgorithm
  eGeoAreaForwardingAlgorithm GeoAreaForwardingAlgorithm;
  /// CbfMinTime
  uint16_t CbfMinTime;
  /// CbfMaxTime
  uint16_t CbfMaxTime;
  /// DefaultMaxCommunicationRange
  uint16_t DefaultMaxCommunicationRange;
  /// UcForwardingPacketBufferSize
  uint16_t UcForwardingPacketBufferSize;
  /// BcForwardingPacketBufferSize
  uint16_t BcForwardingPacketBufferSize;
  /// CbfPacketBufferSize
  uint16_t CbfPacketBufferSize;
  /// TrafficClass
  uint8_t TrafficClass;
} tlibITS_GNMIBParams;

/// Struct for Init parameters
typedef struct libITSNet_InitData_tag
{
  /// MIB parameters (set to NULL to not override any compiled defaults)
  tlibITS_GNMIBParams *pMIBParams;
} tlibITSNet_InitData;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

/**
 * @brief Initiate the ITSNet library
 * @param pData Initial parameters (if required), set to NULL if not required
 * @return Status, 0 for success
 *
 * Calls the initialisation routines and sets up the configuration
 * During the library initialisation the Modem interface is opened
 */
int libITSNet_Init(tlibITSNet_InitData *pData);

/**
 * @brief Start the ITSNet library
 * @return Status, 0 for success
 *
 * Starts all the libraries threads
 * During the library startup the GPS and Security interfaces are opened
 * Once the library is started, the BTP interface is available
 */
int libITSNet_Start(void);

/**
 * @brief Stop the ITSNet library
 * @return Status, 0 for success
 *
 * Stops all the libraries threads
 */
int libITSNet_Stop(void);

/**
 * @brief Finally exit the ITSNet library
 * @return Status, 0 for success
 *
 * Calls the library cleanup routines
 */
int libITSNet_DeInit(void);

/**
 * @brief Request new identity
 * @return Status, 0 for success
 *
 * Triggers creation of new identity
 */
int libITSNet_NewId(void);

#ifdef __cplusplus
}
#endif

#endif // __LIBITSNET_MGMT_H_

// Close the Doxygen group
/**
 * @}
 */
