/**
 * @addtogroup ets_app_facilities_trx_api ETSI ITS Facilities TRX API
 * @{
 *
 * Interface to TRX general controls
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2017 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __ETS_TRX_ETS_TRX_IF_H_
#define __ETS_TRX_ETS_TRX_IF_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// TRX Status codes
typedef enum TRXStatusCode
{
  /// TRX success
  TRX_SUCCESS = 0x00,
  /// TRX generation failure
  TRX_FAILURE = 0x01,

} eTRXStatusCode;

/// Status code for TRX actions @ref eTRXStatusCode
typedef uint8_t tTRXStatusCode;

/// Structure of FAC Tx info for EXT event
/// This is used by the EXT event @c QSMSG_EXT_FAC_TX_INFO
typedef struct TRX_TxInfo
{
  /// Tx power (@sa eETSIGeoNetMaxTxPower)
  int8_t ReqTxPower;
} tTRX_TxInfo;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

/// @brief Set a toll station/protected communications zone
/// @param Identity Identity of protected zone (0 = none)
/// @param Latitude Lat centre of protected zone (tenths-microdegrees)
/// @param Longitude Long centre of protected zone (tenths-microdegrees)
/// @param RadiusM Radius of protected zone (metres, 0 = use default)
/// @param PowerdBm Max allowed transmit power within zone (dBm, 0 = use default)
/// @return TRX Status Code
tTRXStatusCode TRX_SetTollingLocation(uint32_t Identity, int32_t Latitude, int32_t Longitude, uint16_t RadiusM, int8_t PowerdBm);

/// @brief Clear a protected communications zone
/// @return TRX Status Code
tTRXStatusCode TRX_ClearTollingLocation(void);

/// @brief Get the maximum transmit power in half dBm
/// Changes in maximum transmit power reported via EXT interface
/// @return Maximum power in half dBm units
int8_t TRX_GetMaxPower(void);

#ifdef __cplusplus
}
#endif

#endif // __ETS_TRX_ETS_TRX_IF_H_

// Close the Doxygen group
/**
 * @}
 */
