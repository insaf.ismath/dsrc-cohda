#pragma once
/**
 * @addtogroup app_Upgrade
 * @{
 *
 *
 * @file
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2016 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __UPGRADE_H__
#define __UPGRADE_H__

// ----------------------------------------------------------------------------
// Includes
// ----------------------------------------------------------------------------
#include "conf.h"

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

#define QSQUEUE_UPGRADE QSQUEUE_APP5
#define QS_BASE_MSG_UPGRADE QS_BASE_MSG_APP5

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------


/// Supported messages to Upgrade thread
typedef enum
{
  /// Launch Upgrade request
  QSMSG_UPGRADE_START = QS_BASE_MSG_UPGRADE,

  /// Stop Upgrade
  QSMSG_UPGRADE_STOP,

  /// Terminate Upgrade
  QSMSG_UPGRADE_TERMINATE,
} tUpgradeMsgId;

typedef struct UpgradeParam
{
  char* serverAddress;
  char* params;
} tUpgradeParam;

//------------------------------------------------------------------------------
// Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

int  Upgrade_Init(const tCohda_App_Upgrade *pParam);
void Upgrade_Exit(void);

int  Upgrade_FW();

void Upgrade_PrintStats(FILE *pStream);

#if defined(UNITTEST)

#endif

#ifdef __cplusplus
}
#endif

#endif // __UPGRADE_H__
/**
 * @}
 */
