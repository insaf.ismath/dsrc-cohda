/**
 * @addtogroup cohda_ivi_api In Vehicle Information (IVI) API
 * @{
 *
 * @file
 * ivi -rx.h: IVI module main function definitions
 *
 */
//==============================================================================
// Copyright (c) 2015
// Cohda Wireless PTY LTD
//==============================================================================

//------------------------------------------------------------------------------
// Module: IVI Module
//
//------------------------------------------------------------------------------

#ifndef __IVI_H_
#define __IVI_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>
#include <time.h>
#include <stdint.h>
#include <stdbool.h>

#include "ext.h"
#include "debug-levels.h"
#include "qs-lib.h"
#include "conf.h"
#include "util.h"
#include "ldm.h"

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------
int IVI_Init(void);
void IVI_Deinit(void);

#ifdef __cplusplus
}
#endif

#endif // __IVI_H_
// Close the doxygen group
/**
 * @}
 */
