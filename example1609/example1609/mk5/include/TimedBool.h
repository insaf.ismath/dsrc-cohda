//------------------------------------------------------------------------------
// Copyright (c) 2017 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __PLAT_UTIL_TIMEDBOOL_H_
#define __PLAT_UTIL_TIMEDBOOL_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

typedef struct TimedBool
{
  char Dummy[4 * sizeof(struct timeval) + sizeof(uint32_t)
#if (defined(__amd64__)||defined(__aarch64__))
             + 4 // padding in 64-bit machines.
                 // pack the struct?
                 // give up on hiding the data?
#endif
             ];
} tTimedBool;


//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------


void TimedBool_Init(struct TimedBool * pTimedBool,
                    const struct timeval * pMinOn,
                    const struct timeval * pLockout,
                    const struct timeval * pPersist);

void TimedBool_Init_ms(struct TimedBool * pTimedBool,
                       unsigned MinOn_ms,
                       unsigned Lockout_ms,
                       unsigned Persist_ms);

bool TimedBool_Set(struct TimedBool * pTimedBool,
                   const struct timeval * pNow,
                   bool Current);

// should maybe add an optional "const struct timeval * pNow"
bool TimedBool_Get(struct TimedBool * pTimedBool);

// pNow may be NULL
bool TimedBool_Locked(struct TimedBool * pTimedBool,
                      const struct timeval * pNow);

#ifdef __cplusplus
}
#endif

#endif // __PLAT_UTIL_TIMEDBOOL_H_
