#pragma once
/**
 * @addtogroup app_pdmm Probe Data Management Message
 * @{
 *
 * @file
 */

//------------------------------------------------------------------------------
// Copyright (c) 2016 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __PROBE_DATA_MANAGEMENT_MESSAGE_H__
#define __PROBE_DATA_MANAGEMENT_MESSAGE_H__

// ----------------------------------------------------------------------------
// Includes
// ----------------------------------------------------------------------------
#include "conf.h"
#include "j2735asn.h"

#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------
///
typedef enum PDMThreadState
{
  /// Not initialized
  PDM_THREAD_STATE_NONE = 0x00,
  /// Initializing
  PDM_THREAD_STATE_INIT = 0x01,
  /// Running
  PDM_THREAD_STATE_RUN  = 0x02,
  /// Stopping
  PDM_THREAD_STATE_STOP = 0x04,
  /// Stopped
  PDM_THREAD_STATE_END  = 0x08,
} ePDMThreadState;

/// @copydoc ePDMThreadState
typedef int tPDMThreadState;

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------
typedef struct PDMConfiguration
{
  // PDM configuration
  tPDMM PDMConfig;

  // PVDM configuration
  tPVDM PVDMConfig;

  // Snapshot configuration
  tPVDM_Snapshot PDMSnapshotConfig;

  // Event triggering configuration
  tPVDM_Event PDMEventConfig;
} tPDMConfiguration;

typedef struct PDMMState
{
    // Active?
    bool Active;

    /// ID of PDM processing thread
    pthread_t ProcessingThreadID;

    /// PDM processing thread state
    tPDMThreadState ProcessingThreadState;

    // PDM Configuration
    tPDMConfiguration Configuration;
} tPDMMContainer;

//------------------------------------------------------------------------------
// Variables
//------------------------------------------------------------------------------

/// Singleton
extern tPDMMContainer PDMM;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

int  PDMM_Init(void);
void PDMM_Deinit(void);

#ifdef __cplusplus
}
#endif

#endif // __PROBE_DATA_MANAGEMENT_MESSAGE_H__
/**
 * @}
 */
