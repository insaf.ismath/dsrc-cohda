/**
 * 2D points and operations on simple operations on 2D points
 */

//------------------------------------------------------------------------------
// Copyright (c) 2017 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __UTIL_POINT2D_H_
#define __UTIL_POINT2D_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

typedef struct Point2D
{
  /// Northings [mm]
  union {
    float Northings;
    float N;
    float Y;
  };
  // Eastings [mm]
  union {
    float Eastings;
    float E;
    float X;
  };
} tPoint2D;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

/**
 * @brief Translate a point.
 *
 * @param pPoint The point to translate
 * @param X X delta to apply
 * @param Y Y delta to apply
 */
static inline void Translate_Point2D(tPoint2D *pPoint, float X, float Y)
{
  if (pPoint != NULL)
  {
    pPoint->X = pPoint->X + X;
    pPoint->Y = pPoint->Y + Y;
  }
}

void Rotate_Point2D(tPoint2D *pPoint, float angle, const tPoint2D *pPivot);

#ifdef __cplusplus
}
#endif

#endif // __UTIL_POINT2D_H_
// Close the doxygen group
/**
 * @}
 */
