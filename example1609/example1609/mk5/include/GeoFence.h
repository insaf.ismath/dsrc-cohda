#pragma once
/**
 * @addtogroup app_geofence Geofencing functionality
 * @{
 *
 * @file
 */

//------------------------------------------------------------------------------
// Copyright (c) 2016 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __GEOFENCE_H__
#define __GEOFENCE_H__

// ----------------------------------------------------------------------------
// Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------
//Define as fixed
#define MAX_POLYGON_NAME    50


/// Geofence classification result
typedef enum {
  GEOFENCE_INSIDE  =  0, ///< Inside polygon(s)
  GEOFENCE_OUTSIDE =  1, ///< Outside polygon(s)
  GEOFENCE_ERROR   = -1  ///< Internal error
} eGeoFenceResult;

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

// Coordinate tuple
typedef struct GeoFenceCoordinate
{
  float Latitude;
  float Longitude;
} tGeoFenceCoordinate;


// Edge
typedef struct GeoFenceEdge
{
  tGeoFenceCoordinate PointA;
  tGeoFenceCoordinate PointB;

  // The equation of line for which this edge is a line segment
  // latitude = Slope * longitude + Offset
  // (vertical lines are denoted by an infinite slope value)
  float Slope;
  float Offset;

  // Intersected?
  int Intersected;

} tGeoFenceEdge;


/// Polygon
typedef struct GeoFencePolygon
{
  // Bounding box
  tGeoFenceCoordinate Min, Max;
  // Number of entries in the edge table below
  int EdgeCnt;
  // Table of edges
  tGeoFenceEdge *pEdges;
  //Polygon Name
  char polygonName[MAX_POLYGON_NAME + 1];
} tGeoFencePolygon;

/// Intrenal module state values
typedef enum
{
  /// Nothing started
  GEOFENCE_NONE           = 0x00,
  /// Before Util_StartThread()
  GEOFENCE_THREAD_INIT    = 0x02,
  /// If Util_StartThread() failed
  GEOFENCE_THREAD_FAIL    = 0x04,
  /// GeoFence_Thread() started
  GEOFENCE_THREAD_START   = 0x08,
  /// GeoFence_Thread() stop requested
  GEOFENCE_THREAD_STOP    = 0x10,
} eGeoFenceState;

//------------------------------------------------------------------------------
// Variables
//------------------------------------------------------------------------------

typedef void (*GeoFenceApp_Calback)(bool, const char *);

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

int  GeoFence_Init(void);
void GeoFence_Exit(void);

void GeoFence_PrintStats(FILE *pStream);

void GeoFenceApp_RegisterCallback(GeoFenceApp_Calback GeoFenceApp_Callback_ref);

#ifdef __cplusplus
}
#endif

#endif // __GEOFENCE_H__
/**
 * @}
 */
