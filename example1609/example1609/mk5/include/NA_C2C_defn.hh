#pragma once
//------------------------------------------------------------------------------
// Copyright (c) 2018 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#include "NA_C2C_defn.h"

namespace cw
{
namespace NA_C2C
{
  class Conf
  {
  public:
    Conf()
      {
        pConf = NA_C2C_Get();
      };
    virtual ~Conf()
      {
        NA_C2C_Release(&pConf);
      };
    void Update(void)
      {
        NA_C2C_Release(&pConf);
        pConf = NA_C2C_Get();
      };
    operator const tNA_C2C_Cohda_App_CongestionControlJ2945 & () const
      {
        return *pConf;
      };
  private:
    const tNA_C2C_Cohda_App_CongestionControlJ2945 *pConf;
  };
}
}
