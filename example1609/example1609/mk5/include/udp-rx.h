/**
 * @addtogroup udp_rx_api ETSI ITS UDP BTP API
 * @{
 *
 * Define supported messages
 *
 * @file
 *
 * Interface message id definitions
 *
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2010 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __UDPBTP_RX_IF_H_
#define __UDPBTP_RX_IF_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

/// UDP data read buffer
#define UDP_DATA_READ_BUFFER  (4 * 1024)

/// UDP security profile for no security
#define UDPRX_SEC_NONE 0

/// UDP security profile for AID/SSP provided
#define UDPRX_SEC_AID_SSP 1

/// UDP security SSP bits length (in bytes)
#define UDPRX_SEC_SSP_BITS_LENGTH 6

/// UDP security Cert Id length (in bytes)
#define UDPRX_SEC_CERT_ID_LENGTH 8

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// UDP RX message
typedef struct UDPRxMsg
{
  /// length of the packet
  uint32_t len;

  // Do we need other stuff, like source address?

  /// data
  uint8_t  Data[UDP_DATA_READ_BUFFER];
} __attribute__ ((packed)) tUDPRxMsg;


/// UDP BTP Data Req header (with Security)
typedef struct UDPBTPDataReqHdr
{
  /// BTP type
  uint8_t   BTPType;
  /// GN Packet transport type @sa eETSIGeoNetTransport
  uint8_t   PktTransport;
  /// GN Traffic class @sa tETSIGeoNetTC
  uint8_t   TrafficClass;
  /// GN Maximum packet lifetime (sec)
  uint8_t   MaxPktLifetime;

  /// Destination port BTP-A & BTP-B
  uint16_t  DestPort;

  /// BTP-A/B
  union
  {
    /// Source port (BTP-A)
    uint16_t    SrcPort;
    /// Destination port info (BTP-B)
    uint16_t    DestInfo;
  };

  union
  {
    /// Location for GeoUnicast
    struct
    {
      /// Address of destination station
      uint64_t GN_ADDR_as_64;
      // Padding
      uint64_t Unused;
    } Location;
    /// Area for GBC or GAC
    struct
    {
      /// WGS-84 latitude for the center position of shape [1/10 micro degree]
      int32_t Latitude;
      /// WGS-84 longitude for the center position of shape [1/10 micro degree]
      int32_t Longitude;
      /// Distance a of the geometric shape [meters] (always non-zero)
      uint16_t Distance_a;
      /// Distance b of the geometric shape [meters]
      uint16_t Distance_b;
      /// Angle of the geometric shape [degrees from North]
      uint16_t Angle;
      // Shape @sa eETSIGeoNetDestType
      uint8_t Shape;
      // Padding
      uint8_t Unused;
    } Area;
  };

  /// GN Communication profile (see @ref eETSIGeoNetProfile)
  uint8_t CommProfile;
  /// GN Repetition interval (un supported)
  uint8_t RepeatInterval;

  /// GN Security type (None or AID_SSP)
  uint8_t SecProfile;
  /// secure packets (SecProfile == UDPRX_SEC_AID_SSP):
  /// SSP len (0-UDPRX_SEC_SSP_BITS_LENGTH)
  uint8_t SSPLen;
  /// ITS-AID
  uint32_t AID;
  /// SSP Bits
  uint8_t SSPBits[UDPRX_SEC_SSP_BITS_LENGTH];

  /// The length in octets of the 'on-the-air' frame data
  uint16_t Length;
} __attribute__ ((packed)) tUDPBTPDataReqHdr;

/// UDP BTP Data Req message
typedef struct UDPBTPDataReqMsg
{
  /// Message header
  tUDPBTPDataReqHdr Hdr;
  /// Message payload, location
  uint8_t Payload[1];
} __attribute__ ((packed)) tUDPBTPDataReqMsg;


/// UDP BTP Data Ind Header (with Security)
typedef struct UDPBTPDataIndHdr
{
  /// BTP type
  uint8_t   BTPType;
  /// GN Packet transport type @sa eETSIGeoNetTransport
  uint8_t   PktTransport;
  /// GN Traffic class @sa tETSIGeoNetTC
  uint8_t   TrafficClass;
  /// GN Maximum packet lifetime (sec)
  uint8_t   MaxPktLifetime;

  /// Destination port BTP-A & BTP-B
  uint16_t  DestPort;

  /// BTP-A/B
  union
  {
    /// Source port (BTP-A)
    uint16_t    SrcPort;
    /// Destination port info (BTP-B)
    uint16_t    DestInfo;
  };

  union
  {
    /// Location for GeoUnicast
    struct
    {
      /// Address of destination station
      uint64_t GN_ADDR_as_64;
      /// WGS-84 latitude of the GeoAdhoc router expressed in 1/10 micro degree
      int32_t Lat;
      /// WGS84 longitude of the GeoAdhoc router expressed in 1/10 micro degree
      int32_t Long;
    } Location;
    /// Area for GBC or GAC
    struct
    {
      /// WGS-84 latitude for the center position of shape [1/10 micro degree]
      int32_t Latitude;
      /// WGS-84 longitude for the center position of shape [1/10 micro degree]
      int32_t Longitude;
      /// Distance a of the geometric shape [meters] (always non-zero)
      uint16_t Distance_a;
      /// Distance b of the geometric shape [meters]
      uint16_t Distance_b;
      /// Angle of the geometric shape [degrees from North]
      uint16_t Angle;
      // Shape @sa eETSIGeoNetDestType
      uint8_t Shape;
      // Padding
      uint8_t Unused;
    } Area;
  };

  /// GN Security type (None or AID_SSP)
  uint8_t SecProfile;

  /// secure packets (SecProfile == UDPRX_SEC_AID_SSP):
  /// Result of security parsing
  uint8_t ParserResult;
  /// Result of security verification
  uint8_t VerificationResult;
  /// SSP length
  uint8_t SSPLen;

  /// ITS-AID
  uint32_t AID;

  /// SSP bits
  uint8_t SSPBits[UDPRX_SEC_SSP_BITS_LENGTH];

  /// CertID
  uint8_t CertId[UDPRX_SEC_CERT_ID_LENGTH];

  /// The length in octets of the 'on-the-air' frame data
  uint16_t Length;
} __attribute__ ((packed)) tUDPBTPDataIndHdr;

/// UDP BTP Tx message
typedef struct UDPBTPDataIndMsg
{
  /// Message header
  tUDPBTPDataIndHdr Hdr;
  /// Message payload, location
  uint8_t Payload[1];
} __attribute__ ((packed)) tUDPBTPDataIndMsg;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

void UDPBTPRX_ThreadInit (int Policy,
                          int Priority);

void UDPBTPRX_ThreadDeinit (void);

#ifdef __cplusplus
}
#endif

#endif // __UDPBTP_RX_IF_H_

/**
 * @}
 */
