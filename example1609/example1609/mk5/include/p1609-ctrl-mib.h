/**
 * @addtogroup v2x_p1609_ctrl_api
 * @{
 *
 * @file p1609-ctrl-mib.h 1609 MIB information
 *
 * Provides information from the 1609 MIB
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2015 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __P1609_CTRL_MIB_H_
#define __P1609_CTRL_MIB_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

/**
 * @brief Obtains a copy of the continuous radio mac address
 *
 * @param MACAddress Buffer to store the MAC address
 * @return zero for success, negative on error
 */
int P1609Ctrl_MIBGetContinuousMACAddress(uint8_t MACAddress[6]);


/**
 * @brief Obtains a copy of the alternating radio mac address
 *
 * @param MACAddress Buffer to store the MAC address
 * @return zero for success, negative on error
 */
int P1609Ctrl_MIBGetAlternatingMACAddress(uint8_t MACAddress[6]);


/**
 * @brief Obtains the currently used Service Channel number
 *
 * @return the channel number, negative on error
 */
int P1609Ctrl_MIBGetSCH();


/**
 * @brief Obtains the currently used Control Channel number
 *
 * @return the channel number, negative on error
 */
int P1609Ctrl_MIBGetCCH();


/**
 * @brief Obtains the currently used Continuous Channel number
 *
 * @return the channel number, negative on error
 */
int P1609Ctrl_MIBGetLCH();


/**
 * @brief Obtains the currently used transmit power in dBm
 *
 * @return the transmit power in dBm, negative on error
 */
int P1609Ctrl_MIBGetTxPowerLCH();
int P1609Ctrl_MIBGetTxPowerCCH();
int P1609Ctrl_MIBGetTxPowerSCH();

/**
 * @brief Obtains a copy of the the Primary DNS
 *
 * @param PrimaryDNS 16-byte buffer to store the Primary DNS address
 *
 * @return zero for success, negative on error
 */
int P1609Ctrl_MIBGetPrimaryDNS(uint8_t PrimaryDNS[16]);


/**
 * @brief Obtains a copy of the the Secondary DNS
 *
 * @param SecondaryDNS 16-byte buffer to store the Secondary DNS address
 *
 * @return zero for success, negative on error
 */
int P1609Ctrl_MIBGetSecondaryDNS(uint8_t SecondaryDNS[16]);


/**
 * @brief Obtains a copy of the the Gateway MAC
 *
 * @param GatewayMAC 6-byte buffer to store the Gateway MAC address
 *
 * @return zero for success, negative on error
 */
int P1609Ctrl_MIBGetGatewayMAC(uint8_t GatewayMAC[6]);


/**
 * @brief Obtains a copy of the the Default Gateway IPv6 Address
 *
 * @param DefaultGateway 16-byte buffer to store the Default Gateway IPv6 Address
 *
 * @return zero for success, negative on error
 */
int P1609Ctrl_MIBGetDefaultGateway(uint8_t DefaultGateway[16]);

/**
 * @brief Get the channel utilisation for the CCH channel
 *
 * @param[out] pUtilisation A pointer to populate with the current utilisation where 255 = 100%
 *
 * @return zero for success, negative on error
 */
int P1609Ctrl_MIBGetCCHUtilisation(uint8_t *pUtilisation);

/**
 * @brief Get the channel utilisation for the SCH channel
 *
 * @param[out] pUtilisation A pointer to populate with the current utilisation where 255 = 100%
 *
 * @return zero for success, negative on error
 */
int P1609Ctrl_MIBGetSCHUtilisation(uint8_t *pUtilisation);

/**
 * @brief Get the channel utilisation for the safety channel
 *
 * @param[out] pUtilisation A pointer to populate with the current utilisation where 255 = 100%
 *
 * @return zero for success, negative on error
 */
int P1609Ctrl_MIBGetSCHBUtilisation(uint8_t *pUtilisation);

#ifdef __cplusplus
}
#endif

#endif // __P1609_CTRL_MIB_H_

// Close the doxygen group
/**
 * @}
 */

