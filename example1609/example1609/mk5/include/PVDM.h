#pragma once
/**
 * @addtogroup app_pvdm Probe Vehicle Data Message
 * @{
 *
 * @file
 */

//------------------------------------------------------------------------------
// Copyright (c) 2016 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __PROBE_VEHICLE_DATA_MESSAGE_H__
#define __PROBE_VEHICLE_DATA_MESSAGE_H__

// ----------------------------------------------------------------------------
// Includes
// ----------------------------------------------------------------------------
#include "PVDM-cache.h"
#include "conf.h"
#include "j2735asn.h"
#include "lph.h"
#include "vstate.h"

#include <stdio.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Type definitions
//------------------------------------------------------------------------------

/// Data sent to the User callback for PVDM offload.
/// The pData is a pointer to the encoded PVDM message.
typedef struct PVDCallbackData
{
  uint8_t *pData;
  uint16_t Length;
} tPVDCallbackData;

// Client callback called when manually offloading PVD data
typedef void (fPVD_TxCallback) (tPVDCallbackData *pData);

/// Supported return error codes. All errors are negative
typedef enum
{
  /// No error
  PVD_STATUS_OK = 0,
  /// Generic error
  PVD_STATUS_ERROR = -1,
  /// Error code for register API: lock failure
  PVD_STATUS_ELOCK = -101,
  /// Error code for register API: registration errors
  PVD_STATUS_EHANDLE = -102,
} ePVDStatus;

enum
{
  PVD_SEC_OK    = 0,
  PVD_SEC_ERROR
};

// Wiper status sub-types
typedef enum
{
  PVD_WIPER_STATUS_FRONT = 1,
  PVD_WIPER_STATUS_FRONT_RATE,
  PVD_WIPER_STATUS_REAR,
  PVD_WIPER_STATUS_REAR_RATE
} ePVDWiperStatusSubType;

// Brakes status sub-types
typedef enum
{
  PVD_BRAKES_APPLIED = 1,
  PVD_BRAKES_BOOST
} ePVDBrakesStatusSubType;

// Steering angle sub-types
typedef enum
{
  PVD_STEERING_ANGLE = 1,
  PVD_STEERING_ANGLE_CONFIDENCE,
  PVD_STEERING_ANGLE_RATEOFCHANGE,
} ePVDSteeeringAngleSubType;


// FullPos status sub-types
typedef enum
{
  PVD_FULLPOS_LAT = 1,
  PVD_FULLPOS_LON,
  PVD_FULLPOS_ELEV,
  PVD_FULLPOS_HEADING,
  PVD_FULLPOS_SPEED,
  PVD_FULLPOS_TRANSMISSIONSTATE,
} ePVDFullPosStatusSubType;

// Pos2D status sub-types
typedef enum
{
  PVD_POS2D_LAT = 1,
  PVD_POS2D_LON
} ePVDPos2DStatusSubType;

// Pos3D status sub-types
typedef enum
{
  PVD_POS3D_LAT = 1,
  PVD_POS3D_LON,
  PVD_POS3D_ELEV
} PVDPos3DStatusSubType;

/// Message ID Numbers
typedef enum
{
  QSMSG_PVD_MSG_EVENT = QS_BASE_MSG_PVD,
  QSMSG_PVD_PERIOD_EXPIRED,
  QSMSG_PVD_EXT_VSTATE_EVENT,
  QSMSG_PVD_STARTSTOP_EVENT,
  QSMSG_PVD_RX_PDMM,
  QSMSG_PVD_TX
} ePVDEventId;

/// Start Stop states
typedef enum
{
  PVD_STARTSTOP_INIT = 0,
  PVD_STARTSTOP_STOPPING,
  PVD_STARTSTOP_STOPPED,
  PVD_STARTSTOP_STARTED
} ePVDStartStopStates;

///
typedef enum PVDThreadState
{
  /// Not initialized
  PVD_THREAD_STATE_NONE = 0x00,
  /// Initializing
  PVD_THREAD_STATE_INIT = 0x01,
  /// Running
  PVD_THREAD_STATE_RUN  = 0x02,
  /// Stopping
  PVD_THREAD_STATE_STOP = 0x04,
  /// Stopped
  PVD_THREAD_STATE_END  = 0x08,
} ePVDThreadState;

// PVDM offload mode
typedef enum PVDOffloadMode
{
  PVD_OFFLOAD_DSRC_NOTCONNECTED = 1,
  PVD_OFFLOAD_DSRC_CONNECTED = 2,
  PVD_OFFLOAD_CALLBACK = 4
} ePVDOffloadMode;

/// @copydoc ePVDThreadState
typedef int tPVDThreadState;

typedef struct PVDMConfiguration
{
  // PDM configuration
  tPDMM PDMConfig;

  // PVD configuration
  tPVDM PVDConfig;

  // PVD segment number configuration
  tPVDM_PSN PVDSegmentNumberConfig;

  // PVD snapshot configuration
  tPVDM_Snapshot PVDSnapshotConfig;

  // PVDM event triggering configuration
  tPVDM_Event PVDEventConfig;

  // PVD start collection configuration
  tPVDM_StartCollection PVDStartCollectionConfig;

  // PVD start/stop event triggering configuration
  tPVDM_StartStop PVDStartStopEventConfig;
} tPVDMConfiguration;

// Struct to store event time and location, where an event is one of
//    * Periodic Snapshot generation
//    * PSN generation
typedef struct EventTimeLocation
{
  // Last time PSN was generated
  struct timeval Time;

  // Latitude of event
  /// Range: -900000000..900000001 (-90..90) Units: 1/10 micro degree
  int32_t Latitude;
  // Longitude of event
  /// Range -1799999999..1800000001 (-180..180) Units: 1/10 micro degree
  int32_t Longitude;
} tEventTimeLocation;

typedef struct PVDMPSNState
{
  // Last PSN generation time/location
  tEventTimeLocation LastPSNTimeLocation;

  // time jitter - seconds
  uint8_t TimeJitter;

  // distance jitter - metres
  uint8_t DistanceJitter;

  // Current Probe Segment Number
  uint16_t PSN;
} tPVDMPSNState;

typedef struct PVDMSnapshotState
{
  // privacy guard
  bool PrivacyGuard;
  tEventTimeLocation InitialTimeLocation;

  // Last snapshot time/location
  tEventTimeLocation LastSnapshotTimeLocation;

  /// Start/Stop snapshot states
  int StartStopState;
  /// First stop event time
  struct timeval FirstStopEventTime;
  /// Last stop event time
  struct timeval LastStopEventTime;
  /// Last start event time
  struct timeval LastStartEventTime;
} tPVDMSnapshotState;

typedef struct tPVDMEventState
{
  struct PrevState
  {
    tVStateState VState;
    tLPHPos LPHPos;
  } Prev;
} tPVDMEventState;

typedef struct PVDMOffloadState
{
  // Offload Mode - DSRC/Callback
  uint8_t Mode;

  // Last PVDM Offload time
  struct timeval LastOffloadTime;
} tPVDMOffloadState;

typedef struct PVDMState
{
  // destination MAC address for unicast PVDM transmission
  uint8_t DestinationAddress[6];

  // Channel for unicast PVDM transmission
  uint8_t ChannelNumber;

  // Offload State
  tPVDMOffloadState OffloadState;

  // PSN state
  tPVDMPSNState PSNState;

  // Snapshot Cache state
  tPVDMSnapshotCacheState CacheState;

  // Snapshot state
  tPVDMSnapshotState SnapshotState;

  // Event State
  tPVDMEventState EventState;
} tPVDMState;

typedef struct PVDMContainer
{
  // Active?
  bool Active;

  // LPH pointer
  void *pLPH;

  /// ID of PVD processing thread
  pthread_t ProcessingThreadID;

  // ID of PVD periodic thread
  pthread_t PeriodicThreadID;

  // ID of PVD event triggering thread
  pthread_t EventThreadID;

  // PVD processing thread state
  tPVDThreadState ProcessingThreadState;

  // PVD periodic thread state
  tPVDThreadState PeriodicThreadState;

  // PVD event triggering thread state
  tPVDThreadState EventThreadState;

  // Ext Handle
  int ExtHandle;

  // Callback for PVD transmission
  // If set by the PVD client, all messages are offloaded via this callback.
  fPVD_TxCallback *pPVD_TxCallback;

  // configuration
  tPVDMConfiguration Configuration;

  // state
  tPVDMState State;

  // Snapshot cache for period, start/stop and event snapshots
  tPVDMSnapshotCache Cache;

} tPVDMContainer;

//------------------------------------------------------------------------------
// Variables
//------------------------------------------------------------------------------

/// Singleton
extern tPVDMContainer PVDM;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

int  PVDM_Init(fPVD_TxCallback *pCallback);
void PVDM_Deinit(void);
void PVDM_SetConfiguration(tPVDMConfiguration *pConfig);
void PVDM_GetConfiguration(tPVDMConfiguration **ppConfig);

#ifdef __cplusplus
}
#endif

#endif // __PROBE_VEHICLE_DATA_MESSAGE_H__

/**
 * @}
 */
