/**
 * @addtogroup stack_debugging Generic debug printing framework for stack components
 * @{
 *
 * Cohda Wireless Stack Libraries debug printing definitions
 *
 * @file
 *
 *
 */
//------------------------------------------------------------------------------
// Copyright (c) 2013 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __DEBUG_LEVELS_H__
#define __DEBUG_LEVELS_H__

#ifndef D_MASTER
#define D_MASTER (!0)
#endif

#ifndef D_LOCAL
  #define D_LOCAL D_ALL
//#define D_LOCAL D_WARN
#endif

#include "linux/cohda/debug.h"

// Custom debug
#ifndef NO_DEBUG_PRINT_USER
#define d_usertest(var, l) (var >= l)
#define d_userprintf(var, l, f, a...) \
  do { \
    if (var >= l) \
    { \
      struct timespec _Now; \
      clock_gettime(CLOCK_REALTIME, &_Now); \
      fprintf(D_LOGFILE, "[%ld.%09lu] %s: PktDebug: " f, (long)_Now.tv_sec, _Now.tv_nsec,  __FUNCTION__, ## a); \
    } \
  } while (0)
#else
#define d_usertest(var, l) (false)
#define d_userprintf(var, l, f, a...)
#endif

// Some other handy levels
#define D_API    (D_INFO)
#define D_INTERN (D_DEBUG)
#define D_DBG    (D_DEBUG)
#define D_TST    (D_VERBOSE)
//#define D_TST   (D_WARN)

#endif //__DEBUG_LEVELS_H__

// Close the Doxygen group
/**
 * @}
 */

