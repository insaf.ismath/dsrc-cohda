### Configuration for BSM security profile.

psid  0x20

# Interval, in ms, at which full certificates are included in signed messages.
certificate_interval                     450    # BSM profile requires .5 sec.

##############################################################################
### Set which fields are included in the security headers.

include_generation_time                 true    # BSM profile requires true

include_expiration_time                false    # BSM profile requires false

    # Message expiry time is set to generation time plus this value, in ms.
    message_life_time                      0    # arbitrary, not used in BSM

include_generation_location            false    # BSM profile requires false

##############################################################################
### Set which relevance checks are executed.

check_replays                          false    # BSM profile requires false

check_relevance_past_freshness          true    # BSM profile requires true

    # Maximum time in the past, in us, to accept a message as valid.
    past_freshness_tolerance        30000000    # BSM profile requires 30 sec.

check_relevance_future_freshness        true    # BSM profile requires true

    # Maximum time in the future, in us, to accept a message as valid.
    future_freshness_tolerance      30000000    # BSM profile requires 30 sec.

check_relevance_expiry_time            false    # BSM profile requires false

check_relevance_generation_location    false    # BSM profile requires false

    # Maximum distance from receiver, in meters, to accept a message as valid.
    distance_tolerance                     0    # arbitrary, not used in BSM

##############################################################################
### Set which consistency checks are executed.

check_consistency_generation_time       true    # BSM profile requires true

check_consistency_expiry_time          false    # BSM profile requires false

check_consistency_generation_location   false   # BSM profile requires true

##############################################################################
### Peer To Peer Certificate Distribution or Certificate Learning or
### Unknown Certificate Request

# Set which types of certificates are requested if unknown to the receiver.
request_ca_certs                        true    # optional for BSM
request_end_entity_certs               false    # BSM profile requires false

