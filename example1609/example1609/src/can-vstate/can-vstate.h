/**
 * @addtogroup mod_can_vstate CAN + VState Interface Module
 * @{
 *
 * @section mod_can_vstate_dd What Does It Do
 *
 * Registers with @ref can_rx_api to receive a particular incoming CAN message.
 * When a message with this CAN ID is received, requests to update VState with
 * data from the CAN message.
 *
 * @file
 *
 * CAN receiver definitions
 *
 */

//------------------------------------------------------------------------------
// Copyright (c) 2010 Cohda Wireless Pty Ltd
//-----------------------------------------------------------------------------

#ifndef __CAN_VSTATE_H_
#define __CAN_VSTATE_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------
#include <sys/time.h>

#include "dot3-wsmp.h"
#include "j2735-message.h"

//------------------------------------------------------------------------------
// Macros & Constants
//------------------------------------------------------------------------------

/// Location of the CANVState configuration settings
#define CANVSTATE_CONFIG_PATH_NAME  "Example.CANVState"

// Configuration value names
#define CANVSTATE_CONFIG_VALUE_NAME_POLLINGINTERVAL      "PollingInterval"

// Configuration value defaults for mandatory items
#define CANVSTATE_CONFIG_VALUE_DEFAULT_POLLINGINTERVAL           (500)

//-----------------------------------------------------------------------------
// Type Definitions
//-----------------------------------------------------------------------------


///
typedef enum CANVStateThreadState
{
  /// Not initialized
  CANVSTATE_THREAD_STATE_NONE = 0x00,
  /// Initializing
  CANVSTATE_THREAD_STATE_INIT = 0x01,
  /// Running
  CANVSTATE_THREAD_STATE_RUN  = 0x02,
  /// Stopping
  CANVSTATE_THREAD_STATE_STOP = 0x04,
  /// Stopped
  CANVSTATE_THREAD_STATE_END  = 0x08,
} eCANVStateThreadState;
/// @copydoc eCANVStateThreadState
typedef int tCANVStateThreadState;

/// CANVState Statistics
typedef struct CANVStateStats
{
  /// Receive counters
  struct {
    uint32_t Count;
  } Rx;

} tCANVStateStats;

/// CANVState parameters - Stored in Config File
typedef struct CANVStateParams
{
  /// period
  uint32_t PollingInterval; // just for the thread sleep

} tCANVStateParams;

/// CANVState state
typedef struct CANVState
{
  /// ID of CANVState thread
  pthread_t ThreadID;
  /// CANVState thread state
  tCANVStateThreadState ThreadState;
  /// Attributes used for thread
  pthread_attr_t ThreadAttr;
  /// Lock to be used when accessing data
  pthread_mutex_t Lock;
  /// Module statistics
  tCANVStateStats Stats;
  /// Configuration
  tCANVStateParams Params;
  /// Most recently received CAN msg
  tCANRxMsg Msg;
} tCANVState;

//-----------------------------------------------------------------------------
// Function Declarations
//-----------------------------------------------------------------------------

int CANVState_Open (tCANVState **ppCAN,
                pthread_attr_t *pAttr,
                char *pConfigFileName);

void CANVState_Close (tCANVState *pCAN);

void CANVState_PrintStats (tCANVState *pCAN);


#endif // __CAN_VSTATE_H_
/**
 * @}
 */
