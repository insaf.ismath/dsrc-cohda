"""
"""

import os
import libconfig
from LibConfigConverter import conf, dump, NODES
from optparse import OptionParser

MAP = {
    'Cohda.IEEE1609.dot3.WSMP.Priority'         : 'BSMUserPriority',
    'Cohda.IEEE1609.dot4.Control.ChannelNumber' : 'ForcedControlChanNum',
    'Cohda.IEEE1609.dot4.Safety.ChannelNumber'  : 'ContinuousChanNum',
    'Cohda.IEEE1609.dot4.Service.ChannelNumber' : 'ForcedSerChanNum',
    'Cohda.Vehicle.AntAdjX_m'                   : 'AntAdjX_m',
    'Cohda.Vehicle.AntAdjY_m'                   : 'AntAdjY_m',
    'Cohda.Vehicle.AntAdjZ_m'                   : 'AntAdjZ_m'
    }

PREFIX = {
    'Cohda.IEEE1609.dot3.WSMP.'   : 'WSMP_',
    'Cohda.IEEE1609.dot4.Control.': 'CCH_',
    'Cohda.IEEE1609.dot4.Service.': 'SCH_',
    'Cohda.Vehicle.'              : 'Vehicle'
}
for i in range(32):
    PREFIX['Cohda.Provider.Services.[{0}].'.format(i)] = 'WBSS_Service_{0}_'.format(i)
    PREFIX['Cohda.User.Services.[{0}].'.format(i)] = 'WBSS_Service_{0}_'.format(i)

NODES += (
    'Cohda.Provider.Services',
    'Cohda.User.Services',
    'Cohda.Vehicle',
    )

IGNORE = (
    'Cohda.IEEE1609.dot2.CertificateStore',
    'Cohda.IEEE1609.dot2.MaxNumCerts',
    'Cohda.IEEE1609.dot2.RequestCertsLevel',
    )

usage = """usage: %prog [options] <ASD.cfg>
  only support config file inlibconfig syntax"""

class newconf(conf):
    def __init__(self):
        conf.__init__(self)

    def readFile(self, fn):
        libconfig.Config.readFile(self, fn)
        for node in NODES:
            self._parse(node)

def main():
    parser = OptionParser(usage=usage)
    parser.add_option("-p", "--pre", dest="pre_conf",
                      help="file to be prepend to the output file",
                      metavar="FILE")
    parser.add_option("-s", "--post", dest="post_conf",
                      help="file to be append to the output file",
                      metavar="FILE")
    (options, args) = parser.parse_args()
    if not len(args) == 1:
        parser.error("Must specify one file to convert")
    fn = args[0]
    if not os.path.isfile(fn):
        parser.error("{0} is not a valid file".format(fn))
    c = newconf()
    c.readFile(fn)
    dump(options.pre_conf)
    for (key,typ,val) in c.all_children:
        # name = i[0]
        name = key.split('.')[-1]
        value = str(val)

        # Prefixes
        for p in PREFIX:
            if (key.startswith(p)):
                name = PREFIX[p] + name
        ## map
        if key in MAP:
            name = MAP[key]
        ## ignore list
        elif key in IGNORE:
            continue
        print('{0:<25s} = {1:>18s} # {2:s}'.format(name, value, key))
    print('\n#end  <%s>\n' % fn)
    dump(options.post_conf)

if __name__=='__main__':
    main()
